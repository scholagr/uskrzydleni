[_metadata_:partType]:- "STROPHE"
**1.**  Duchu Święty wołam przyjdź  
bądź jak ogień duszy mej  
bądź jak ogień w ciele mym  
rozpal mnie.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Wszechmogący Bóg jest pośród nas,   
miłosierdzie Jego wielkie jest.  
Okazuje dobroć swoją dziś  
dla tych, którzy chcą miłować Go.*


