[_metadata_:partType]:- "STROPHE"
**1.**  Panie, któż jak Ty, od wieków jest Twój tron,  
na wieki są Twe dni.  
Morza chwalą Cię, góry wznoszą pieśń,  
Tyś najwyższy jest x2

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Bóg Adonai, od poranka aż po zmierzch  
cała Ziemia Tobie śpiewa  
Bóg Adonai, całe Niebo chwali Cię,  
Jedynemu Bogu chwała cześć.*

[_metadata_:partType]:- "STROPHE"
**2.**  Panie, któż jak Ty, od wieków jest Twój tron,  
na wieki są Twe dni.  
Morza chwalą Cię, góry wznoszą pieśń,  
Tyś najwyższy jest x2

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Bóg Adonai, od poranka aż po zmierzch  
cała Ziemia Tobie śpiewa  
Bóg Adonai, całe Niebo chwali Cię,  
Jedynemu Bogu chwała  
Bóg Adonai, od poranka aż po zmierzch  
cała Ziemia Tobie śpiewa  
Bóg Adonai, całe Niebo chwali Cię,  
Jedynemu Bogu chwała, cześć.*


