[_metadata_:partType]:- "STROPHE"
**1.**  Dzisiaj w Betlejem, dzisiaj w Betlejem   
wesoła nowina,   
że Panna czysta, że Panna czysta   
porodziła Syna.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chrystus się rodzi, nas oswobodzi,  
Anieli grają, króle witają,  
pasterze śpiewają, bydlęta klękają,  
cuda, cuda ogłaszają.*

[_metadata_:partType]:- "STROPHE"
**2.**  Maryja Panna, Maryja Panna   
Dzieciątko piastuje,  
i Józef święty, i Józef święty  
Ono pielęgnuje.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chrystus się rodzi, nas oswobodzi,  
Anieli grają, króle witają,  
pasterze śpiewają, bydlęta klękają,  
cuda, cuda ogłaszają.*

[_metadata_:partType]:- "STROPHE"
**3.**  Choć w stajeneczce, choć w stajeneczce   
Panna Syna rodzi  
Przecież On wkrótce, przecież On wkrótce   
ludzi oswobodzi

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chrystus się rodzi, nas oswobodzi,  
Anieli grają, króle witają,  
pasterze śpiewają, bydlęta klękają,  
cuda, cuda ogłaszają.*

[_metadata_:partType]:- "STROPHE"
**4.**  I Trzej Królowie, i Trzej Królowie   
od wschodu przybyli  
I dary Panu, i dary Panu   
kosztowne złożyli

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chrystus się rodzi, nas oswobodzi,  
Anieli grają, króle witają,  
pasterze śpiewają, bydlęta klękają,  
cuda, cuda ogłaszają.*

[_metadata_:partType]:- "STROPHE"
**5.**  Pójdźmy też i my, pójdźmy też i my   
przywitać Jezusa  
Króla nad królmi, Króla nad królmi   
uwielbić Chrystusa

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chrystus się rodzi, nas oswobodzi,  
Anieli grają, króle witają,  
pasterze śpiewają, bydlęta klękają,  
cuda, cuda ogłaszają.*

[_metadata_:partType]:- "STROPHE"
**6.**  Bądźże pochwalon, bądźże pochwalon   
dziś, nasz wieczny Panie  
Któryś złożony, któryś złożony   
na zielonym sianie

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chrystus się rodzi, nas oswobodzi,  
Anieli grają, króle witają,  
pasterze śpiewają, bydlęta klękają,  
cuda, cuda ogłaszają.*

[_metadata_:partType]:- "STROPHE"
**7.**  Bądź pozdrowiony, bądź pozdrowiony   
Boże nieskończony  
Sławimy Ciebie, sławimy Ciebie,   
Boże niezmierzony

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chrystus się rodzi, nas oswobodzi,  
Anieli grają, króle witają,  
pasterze śpiewają, bydlęta klękają,  
cuda, cuda ogłaszają.*


