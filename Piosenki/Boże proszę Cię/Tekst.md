[_metadata_:partType]:- "STROPHE"
**1.**  Boże proszę Cię,   
naucz miłości Twej  
Człowieka upadłego   
ciągle szukającego

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Proszę Cię: Daj nam promyk światła, który  
ożywi nas  
Który da siłę i wytrwanie jakie  
umocnią nas*

[_metadata_:partType]:- "STROPHE"
**2.**  Szukam Ciebie stale,   
wśród kwiatów i chmur nawale  
W słońcu, w pękach róż   
i wielkich otchłaniach mórz

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Proszę Cię: Daj nam promyk światła, który  
ożywi nas  
Który da siłę i wytrwanie jakie  
umocnią nas*

[_metadata_:partType]:- "STROPHE"
**3.**  Teraz jesteśmy sami   
i Ciebie wciąż szukamy  
Zgubieni w szarym tłumie,   
gdzie brat drugiego nie rozumie

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Proszę Cię: Daj nam promyk światła, który  
ożywi nas  
Który da siłę i wytrwanie jakie  
umocnią nas*


