[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Powstań o Panie,   
ocal mnie, mój Boże!  
Mocą napełnij me serce!*

[_metadata_:partType]:- "STROPHE"
**1.**  Któż uwierzy temu, cośmy usłyszeli,   
na kim się ramię Pańskie objawiło?   
On wyrósł przed nami jak młode drzewo   
i jakby korzeń z wyschniętej ziemi.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Powstań o Panie,   
ocal mnie, mój Boże!  
Mocą napełnij me serce!*

[_metadata_:partType]:- "STROPHE"
**2.**  Nie miał On wdzięku ani też blasku,  
aby na Niego popatrzeć;  
wzgardzony i odepchnięty przez ludzi;  
nie miał wyglądu, by się nam podobał.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Powstań o Panie,   
ocal mnie, mój Boże!  
Mocą napełnij me serce!*

[_metadata_:partType]:- "STROPHE"
**3.**  Mąż boleści, oswojony z cierpieniem,  
jak ktoś, przed kim się twarze zakrywa;  
wzgardzony tak, iż mieliśmy Go za nic,  
za skazańca żeśmy Go uznali.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Powstań o Panie,   
ocal mnie, mój Boże!  
Mocą napełnij me serce!*

[_metadata_:partType]:- "STROPHE"
**4.**  Lecz On się obarczył naszym cierpieniem,  
On dźwigał nasze boleści,  
On był przebity za nasze grzechy,  
i zdruzgotany za nasze winy.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Powstań o Panie,   
ocal mnie, mój Boże!  
Mocą napełnij me serce!*

[_metadata_:partType]:- "STROPHE"
**5.**  Chłosta zbawienna dla nas Nań spadła,  
a w Jego ranach jest nasze zdrowie;  
wszyscyśmy pobłądzili jak owce;  
każdy z nas się obrócił ku własnej drodze.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Powstań o Panie,   
ocal mnie, mój Boże!  
Mocą napełnij me serce!*

[_metadata_:partType]:- "STROPHE"
**6.**  Pan zwalił na Niego winy nas wszystkich,  
lecz On sam się dał gnębić, gdy Go dręczono  
jak baranek na rzeź prowadzony,  
jak owca niema ust swych nie otworzył.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Powstań o Panie,   
ocal mnie, mój Boże!  
Mocą napełnij me serce!*

[_metadata_:partType]:- "STROPHE"
**7.**  Chociaż nikomu krzywdy nie wyrządził  
i w Jego ustach kłamstwo nie powstało  
spodobało się Panu zmiażdżyć Go cierpieniem,  
by wola Pańska spełniła się przez Niego.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Powstań o Panie,   
ocal mnie, mój Boże!  
Mocą napełnij me serce!*

[_metadata_:partType]:- "STROPHE"
**8.**  Po udrękach swojej duszy  
ujrzy światło i nim się nasyci;  
zacny Sługa wielu usprawiedliwi,  
ich nieprawości On sam dźwigać będzie.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Powstań o Panie,   
ocal mnie, mój Boże!  
Mocą napełnij me serce!*

[_metadata_:partType]:- "STROPHE"
**9.**  Dlatego w nagrodę Bóg Mu przydzielił  
tłumy, za które na śmierć się ofiarował,  
bo On poniósł grzechy wielu  
i oręduje za nami.


