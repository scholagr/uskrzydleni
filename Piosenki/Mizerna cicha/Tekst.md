[_metadata_:partType]:- "STROPHE"
**1.**  Mizerna, cicha, stajenka licha,  
Pełna niebieskiej chwały.  
Oto leżący, przed nami śpiący  
W promieniach Jezus mały.

[_metadata_:partType]:- "STROPHE"
**2.**  Nad nim anieli w locie stanęli  
I pochyleni klęczą  
Z włosy złotymi, z skrzydły białymi,  
Pod malowaną tęczą.

[_metadata_:partType]:- "STROPHE"
**3.**  Wielkie zdziwienie, wszelkie stworzenie  
Cały świat orzeźiony;  
Mądrość Mądrości, Światłość Światłości,  
Bóg - człowiek tu wcielony!

[_metadata_:partType]:- "STROPHE"
**4.**  I oto mnodzy, ludzie ubodzy  
Radzi oglądać Pana,  
Pełni natchnienia, pełni zdziwienia  
Upadli na kolana.

[_metadata_:partType]:- "STROPHE"
**5.**  Oto Maryja, czysta lilija,  
Przy niej staruszek drżący  
Stoją przed nami, przed pastuszkami  
Tacy uśmiechający.


