[_metadata_:partType]:- "STROPHE"
**1.**  Nadeszła, Ojcze, godzina moja,  
Otocz Mnie chwałą, bym wielbił Cię,  
Bym Twoją mocą dał życie na wieki  
Tym, których, Ojcze, oddałeś mi.

[_metadata_:partType]:- "STROPHE"
**2.**  Twe Imię, Ojcze, im objawiłem,  
To Twoje dzieci, któreś mi dał,  
A oni wierzą we wszystkie twe słowa,  
Za nimi proszę, abyś ich strzegł.

[_metadata_:partType]:- "STROPHE"
**3.**  Świat jest im wrogiem, bo nie zna Ciebie,  
Jak Mnie posłałeś, posyłam ich,  
Uświęć ich w prawdzie, twe słowo jest Prawdą,  
Za nich w ofierze poświęcam się.

[_metadata_:partType]:- "STROPHE"
**4.**  Niech będą jedno, jak my jesteśmy,  
Ja Ojcze w Tobie, a we Mnie Ty,  
Aby świat poznał, że Ty Mnie posłałeś,  
Że Ty ich kochasz, jak kochasz Mnie.

[_metadata_:partType]:- "STROPHE"
**5.**  Chcę także, Ojcze, których mi dałeś,  
Zabrać ze sobą do chwały Twej.  
Niechaj zobaczą, jak bardzo Mnie kochasz,  
By szczęście nasze mieszkało w nich.


