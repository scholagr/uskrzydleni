[_metadata_:partType]:- "STROPHE"
**1.**  Bo jak śmierć, potężna jest miłość,  
a zazdrość jej nieprzejednana jak Szeol.  
Żar jej to żar ognia,   
płomień Pański.  
Wody wielkie nie zdołają ugasić miłości,  
nie zatopią, nie zatopią jej rzeki.


