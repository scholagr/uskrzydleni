[_metadata_:partType]:- "STROPHE"
**1.**  Archanioł Boży Gabriel,  
Posłan do Panny Maryi,  
Z Majestatu Trójcy Świętej,  
Tak sprawował poselstwo k`Niej  
Zdrowaś, Panno, łaskiś pełna,  
Pan jest z Tobą, to rzecz pewna".

[_metadata_:partType]:- "STROPHE"
**2.**  Panna się wielce zdumiała  
z poselstwa, które słyszała.  
Pokorniuchno się skłoniła;  
jako Panna wstrzemięźliwa  
zasmuciła się z tej mowy,  
nic nie rzekła Aniołowi.

[_metadata_:partType]:- "STROPHE"
**3.**  Ale poseł z wysokości,  
napełnion Boskiej mądrości,  
rzekł Jej: Nie bój się Maryjo,  
najszczęśliwszaś, Panno miła,  
znalazłaś łaskę u Pana,  
oto poczniesz Jego Syna.

[_metadata_:partType]:- "STROPHE"
**4.**  Jezus nazwiesz imię Jego,  
będzie Synem Najwyższego;  
wielki z strony człowieczeństwa,  
a niezmierny z strony Bóstwa,  
wieczny Syn Ojca Wiecznego,  
Zbawiciel świata całego".

[_metadata_:partType]:- "STROPHE"
**5.**  Temu Panna uwierzyła,  
przyzwalając tak mówiła:  
O, Pośle Boga wiecznego,  
gdy to wola Pana mego,  
toć ja służebnica Jego,  
stań się według słowa twego"


