[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Oddaję Ci życie swe,   
Ty Panie wiesz, jakie jest.   
Za wszystko przepraszam Cię,   
co było grzechem i złem.*

[_metadata_:partType]:- "STROPHE"
**1.**  Oczyść Panie dusze mą,   
zapomnij mi moje winy.  
Obmyj mnie Swą świętą krwią,   
podaruj mi nowe życie.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Daj Ducha Świętego mi,   
by odtąd prowadził mnie,  
rozpalił dziś w sercu mym   
ognisko miłości Twej.*

[_metadata_:partType]:- "STROPHE"
**2.**  Przyjdź, zamieszkaj w domu mym,   
z Twą łaską i miłosierdziem.  
Otocz mnie ramieniem Swym,   
bo ja

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Oddaję Ci życie swe,   
Ty Panie wiesz, jakie jest.   
Za wszystko przepraszam Cię,   
co było grzechem i złem.*

[_metadata_:partType]:- "STROPHE"
**3.**  Wybawiłeś duszę ma,   
umarłeś za moje winy.  
Jezu jesteś Panem mym,   
Zbawicielem mym

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Daj Ducha Świętego mi,   
by odtąd prowadził mnie,  
rozpalił dziś w sercu mym   
ognisko miłości Twej.*


