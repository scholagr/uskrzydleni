[_metadata_:partType]:- "STROPHE"
**1.**  Schowaj mnie pod skrzydła Swe  
Ukryj mnie w silnej dłoni Swej

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Kiedy fale mórz chcą porwać mnie  
Z Tobą wzniosę się, podniesiesz mnie  
Panie Królem Tyś spienionych wód  
Ja ufam Ci - Ty jesteś Bóg.  
Ja ufam Ci - Ty jesteś Bóg.*

[_metadata_:partType]:- "STROPHE"
**2.**  Odpocznę dziś w ramionach Twych  
Dusza ma w Tobie będzie trwać.


