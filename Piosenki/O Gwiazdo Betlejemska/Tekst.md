[_metadata_:partType]:- "STROPHE"
**1.**  O Gwiazdo Betlejemska zaświeć nad niebem mym.  
Tak szukam cię wśród nocy tęsknię za światłem twym  
Zaprowadź do stajenki, leży tam Boży Syn,  
Bóg Człowiek z Panny Świętej, dany na okup win

[_metadata_:partType]:- "STROPHE"
**2.**  O, nie masz Go już w szopce, nie masz Go w żłóbku tam  
Więc gdzie pójdziemy Chryste? Gdzie się ukryłeś nam?  
Pójdziemy przed ołtarze wzniecić miłości żar  
I hołd Ci niski oddać, to jest nasz wszystek dar.

[_metadata_:partType]:- "STROPHE"
**3.**  Ja nie wiem, o mój Panie, któryś miał w żłobie tron,  
Czy dusza moja biedna milsza Ci jest niż on.  
Ulituj się nade mną, błagać Cię kornie śmiem,  
Gdyś stajnią nie pogardził, nie gardź i sercem mym.


