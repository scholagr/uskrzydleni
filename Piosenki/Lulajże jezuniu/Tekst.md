[_metadata_:partType]:- "STROPHE"
**1.**  Lulajże Jezuniu, moja Perełko,  
Lulaj ulubione me Pieścidełko.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Lulajże Jezuniu, lulajże lulaj!  
A Ty Go, Matulu, w płaczu utulaj.*

[_metadata_:partType]:- "STROPHE"
**2.**  Zamknijże znużone płaczem powieczki,  
Utulże zemdlone łkaniem wardżeczki.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Lulajże Jezuniu, lulajże lulaj!  
A Ty Go, Matulu, w płaczu utulaj.*

[_metadata_:partType]:- "STROPHE"
**3.**  Lulajże, przyjemna oczom Gwiazdeczko,  
Lulaj, najśliczniejsze świata Słoneczko.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Lulajże Jezuniu, lulajże lulaj!  
A Ty Go, Matulu, w płaczu utulaj.*

[_metadata_:partType]:- "STROPHE"
**4.**  My z Tobą, tam w niebie, spocząć pragniemy,  
Ciebie, tu na ziemi, kochać będziemy.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Lulajże Jezuniu, lulajże lulaj!  
A Ty Go, Matulu, w płaczu utulaj.*

[_metadata_:partType]:- "STROPHE"
**5.**  Lulajże, piękniuchny nasz Aniołeczku.  
Lulajże wdzięczniuchny świata Kwiateczku.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Lulajże Jezuniu, lulajże lulaj!  
A Ty Go, Matulu, w płaczu utulaj.*

[_metadata_:partType]:- "STROPHE"
**6.**  Lulajże, Różyczko najozdobniejsza,  
Lulajże, Lilijko najprzyjemniejsza.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Lulajże Jezuniu, lulajże lulaj!  
A Ty Go, Matulu, w płaczu utulaj.*


