[_metadata_:partType]:- "STROPHE"
**1.**  Jest na świecie miłość,   
której imię srebrzystym promieniem objęło ziemię.  
Gdy Ci będzie smutno,   
imię to wypowiedz, a przy tobie Ona zjawi się.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Matka z radością poda dłoń,   
gdy powiesz: "Mario, w Tobie ufność mam.   
Kiedy jesteś przy mnie, znika ból i w oczach łzy,  
Pragnę z Tobą być, Ty dodaj sił."*

[_metadata_:partType]:- "STROPHE"
**2.**  Jest w Niej tyle ciepła, a w spojrzeniu Jej   
dobroć i troska Matczyna gości.  
Otwórz serce swoje, schowaj Ją głęboko   
i jak dziecko powiedz: "Kocham Cię".

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Matka z radością poda dłoń,   
gdy powiesz: "Mario, w Tobie ufność mam.   
Kiedy jesteś przy mnie, znika ból i w oczach łzy,  
Pragnę z Tobą być, Ty dodaj sił."*

[_metadata_:partType]:- "STROPHE"
**3.**  Wtul się w jej ramiona a pokochasz Ją mocno   
i nie będziesz chciał odejść od niej   
daj Jej swoje ręce a zobaczysz słońce   
i nadzieję Mario Kocham Cię.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Matka z radością poda dłoń,   
gdy powiesz: "Mario, w Tobie ufność mam.   
Kiedy jesteś przy mnie, znika ból i w oczach łzy,  
Pragnę z Tobą być, Ty dodaj sił."*


