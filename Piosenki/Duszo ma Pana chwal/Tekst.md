[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Duszo ma, Pana chwal, oddaj Bogu cześć  
Świętemu śpiewaj pieśń.	  
Z mocą wywyższaj go, duszo ma	   
Uwielbiam Boże Cię.*

[_metadata_:partType]:- "STROPHE"
**1.**  Nowy dzień wraz ze wschodem słońca,	   
Znów nadszedł czas by Ci śpiewać pieśń.	   
Cokolwiek jest już za mną i to wszystko co przede mną.   
Wiem będę śpiewać, gdy nadejdzie zmrok.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Duszo ma, Pana chwal, oddaj Bogu cześć  
Świętemu śpiewaj pieśń.	  
Z mocą wywyższaj go, duszo ma	   
Uwielbiam Boże Cię.*

[_metadata_:partType]:- "STROPHE"
**2.**  Bogaty w miłość gniew oddalasz, Panie.  
Twe serce miłe, wielkie Imię Twe  
Ze względu na Twą dobroć będę śpiewać Tobie pieśni,  
Wiele powodów, by uwielbiać Cię.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Duszo ma, Pana chwal, oddaj Bogu cześć  
Świętemu śpiewaj pieśń.	  
Z mocą wywyższaj go, duszo ma	   
Uwielbiam Boże Cię.*

[_metadata_:partType]:- "STROPHE"
**3.**  Przyjdzie dzień, gdy bez sił zostanę,  
Nadejdzie czas mego końca tu.  
Dusza ma będzie już na zawsze Cię uwielbiać.  
W wieczności z Tobą piękna zabrzmi pieśń.


