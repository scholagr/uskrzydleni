[_metadata_:partType]:- "STROPHE"
**1.**  Pójdźmy wszyscy do stajenki,  
do Jezusa i Panienki  
Powitajmy, Maleńkiego  
I Maryję Matkę jego.  
Powitajmy, Maleńkiego  
I Maryję Matkę jego.

[_metadata_:partType]:- "STROPHE"
**2.**  Witaj, Jezu ukochany,  
Od Patryjarchów czekany,  
Od Proroków ogłoszony,  
Od narodów upragniony.

[_metadata_:partType]:- "STROPHE"
**3.**  Witaj, Dziecinko w żłobie,  
Wyznajemy Boga w Tobie  
Coś się narodził tej nocy  
Byś nas wyrwał z czarta mocy.

[_metadata_:partType]:- "STROPHE"
**4.**  Witaj Jezu nam zjawiony:  
Witaj dwakroć narodzony,  
Raz z ojca przed wieków wiekiem,  
A teraz z matki człowiekiem

[_metadata_:partType]:- "STROPHE"
**5.**  Któż to słyszał takie dziwy?  
Tyś człowiek i Bóg prawdziwy,  
Ty łączysz w boskiej osobie  
Dwie natury różne sobie

[_metadata_:partType]:- "STROPHE"
**6.**  O, szczęśliwi pastuszkowie,  
Któż radość waszą wypowie?  
Czego ojcowie żądali,  
Byście pierwsi oglądali.

[_metadata_:partType]:- "STROPHE"
**7.**  O Jezu nasze kochanie,  
Czemu nad niebios mieszkanie  
Przekłądasz nędzę, ubóstwo,  
I wyniszczesz swoje Bóśtwo?


