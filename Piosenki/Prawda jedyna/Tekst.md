[_metadata_:partType]:- "STROPHE"
**1.**  Prawda jedyna słowa Jezusa z Nazaretu   
że swego syna posłał z nieba Bóg na świat.  
Aby niewinnie cierpiąc zmarł za nasze grzechy   
I w pohańbieniu przyjął winy wszystkich nas.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Dzięki ci Boże mój   
za ten krzyż   
Który Jezus cierpiał za mnie,  
Jezus cierpiał za mnie.*

[_metadata_:partType]:- "STROPHE"
**2.**  Uwierz w Jezusa przecież On za ciebie umarł  
Z miłości do nas przyszedł z nieba na ten świat  
Błogosławiony ten kto wierzy choć nie widział  
Zaufaj dziś Bogu a na wieki będziesz żył.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Dzięki ci Boże mój   
za ten krzyż   
Który Jezus cierpiał za mnie,  
Jezus cierpiał za mnie.*


