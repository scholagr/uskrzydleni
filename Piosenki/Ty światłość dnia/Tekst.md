[_metadata_:partType]:- "STROPHE"
**1.**  Ty światłość dnia wszedłeś w moje ciemności,  
dałeś mi wzrok, abym mógł  
widzieć Twą Twarz i Spojrzenie Miłości,  
którym roztapiasz mój strach

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Jestem tu, by wielbić,   
by oddawać chwałę,  
jestem tu, by wyznać:   
to mój Bóg.  
dobry i łaskawy,   
cały tak wspaniały,  
ponad wszystko cenny   
dla mnie jest.*

[_metadata_:partType]:- "STROPHE"
**2.**  Ty czasów Król, wywyższony na wieki,  
jaśnieje w Niebie Twój Tron  
zszedłeś na Ziemię, by stać się Człowiekiem,  
by rajem stał się mój dom

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Jestem tu, by wielbić,   
by oddawać chwałę,  
jestem tu, by wyznać:   
to mój Bóg.  
dobry i łaskawy,   
cały tak wspaniały,  
ponad wszystko cenny   
dla mnie jest.*

[_metadata_:partType]:- "BRIDGE"
**Bridge**  I niczym nie odpłacę się  
za miłość Twą i za Twój Krzyż! x3

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Jestem tu, by wielbić,   
by oddawać chwałę,  
jestem tu, by wyznać:   
to mój Bóg.  
dobry i łaskawy,   
cały tak wspaniały,  
ponad wszystko cenny   
dla mnie jest.*


