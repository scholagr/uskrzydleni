[_metadata_:partType]:- "STROPHE"
**1.**  Wysławiać będę Ciebie, Panie  
Żeś podniósł mnie  
A nie dałeś moim wrogom  
Triumfować nade mną

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Panie i Boże mój wołam do Ciebie  
A Tyś mnie uzdrowił // 2x*

[_metadata_:partType]:- "STROPHE"
**2.**  Panie, wywiodłeś duszę moją  
Z otchłani  
Zachowałeś mnie żywym między  
Zstępujących do grobu

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Panie i Boże mój wołam do Ciebie  
A Tyś mnie uzdrowił // 2x*

[_metadata_:partType]:- "STROPHE"
**3.**  Śpiewajcie Panu,   
wierni Pańscy  
Chwalcie Pana  
W Jego świętości

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Panie i Boże mój wołam do Ciebie  
A Tyś mnie uzdrowił // 2x  
  
Panie i Boże mój wołam do Ciebie // 3x  
A Tyś mnie uzdrowił*


