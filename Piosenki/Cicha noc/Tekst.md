[_metadata_:partType]:- "STROPHE"
**1.**  Cicha noc, święta noc,  
Pokój niesie ludziom wszem.  
A u żłóbka Matka Święta,  
Czuwa sama uśmiechnięta.  
Nad Dzieciątka snem,  
Nad Dzieciątka snem.

[_metadata_:partType]:- "STROPHE"
**2.**  Cicha noc, święta noc,  
Pastuszkowie od swych trzód  
Biegną wielce zadziwieni,  
Za Anielskim głosem pieni,  
Gdzie się spełnił cud,  
Gdzie się spełnił cud.

[_metadata_:partType]:- "STROPHE"
**3.**  Cicha noc, święta noc,  
Narodzony Boży Syn.  
Pan Wielkiego Majestatu,  
Niesie dziś całemu światu,  
Odkupienie win,  
Odkupienie win.

[_metadata_:partType]:- "STROPHE"
**4.**  Cicha noc, święta noc,  
Jakiż w Tobie dzisiaj cud,  
W Betlejem dziecina święta  
Wznosi w górę swe rączęta  
Błogosławi lud  
Błogosławi lud.


