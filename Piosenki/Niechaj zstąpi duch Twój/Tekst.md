[_metadata_:partType]:- "STROPHE"
**1.**  Niechaj zstąpi Duch Twój i odnowi ziemię.   
Życiodajny spłynie deszcz na spragnione serce.   
Obmyj mnie i uświęć mnie,   
uwielbienia niech popłynie pieśń. /2x

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chwała Jezusowi, który za mnie życie dał.   
Chwała Temu, który pierwszy umiłował mnie.   
Jezus, tylko Jezus Panem jest.*


