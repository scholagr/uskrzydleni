[_metadata_:partType]:- "STROPHE"
**1.**  Pan kiedyś stanął nad brzegiem,  
Szukał ludzi gotowych pójść za Nim;  
By łowić serca słów Bożych prawdą

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *O Panie, to Ty na mnie spojrzałeś,  
Twoje usta dziś wyrzekły me imię.  
Swoją barkę pozostawiam na brzegu,  
Razem z Tobą nowy zacznę dziś łów.*

[_metadata_:partType]:- "STROPHE"
**2.**  Jestem ubogim człowiekiem,  
Moim skarbem są ręce gotowe  
Do pracy z Tobą i czyste serce.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  **

[_metadata_:partType]:- "STROPHE"
**3.**  Ty, potrzebujesz mych dłoni,  
Mego serca młodego zapałem  
Mych kropli potu i samotności.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  **

[_metadata_:partType]:- "STROPHE"
**4.**  Dziś wypłyniemy już razem  
Łowić serca na morzach dusz ludzkich   
Twej prawdy siecią i słowem życia.


