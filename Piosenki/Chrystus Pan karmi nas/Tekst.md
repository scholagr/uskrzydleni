[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chrystus Pan karmi nas  
Swoim świętym Ciałem,  
Chwalmy Go na wieki!*

[_metadata_:partType]:- "STROPHE"
**1.**  Duchem całym wielbię Pana,  
Boga, Zbawcę jedynego,  
Bo w Nim samym odnajduję  
Wszelką radość życia mego.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chrystus Pan karmi nas  
Swoim świętym Ciałem,  
Chwalmy Go na wieki!*

[_metadata_:partType]:- "STROPHE"
**2.**  Sprawił we mnie wielkie dzieła  
W Swej dobroci niepojętej.  
On Wszechmocny, On Najwyższy,  
On sam jeden zawsze Święty.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chrystus Pan karmi nas  
Swoim świętym Ciałem,  
Chwalmy Go na wieki!*

[_metadata_:partType]:- "STROPHE"
**3.**  Wielbię, bo chciał wejrzeć z nieba  
Na swą sługę uniżoną,  
By mnie odtąd wszyscy ludzie  
Mogli zwać błogosławioną.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chrystus Pan karmi nas  
Swoim świętym Ciałem,  
Chwalmy Go na wieki!*

[_metadata_:partType]:- "STROPHE"
**4.**  On, który przez pokolenia  
Pozostaje miłosierny  
Wobec tego, kto Mu służy  
I chce zostać Jemu wierny.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chrystus Pan karmi nas  
Swoim świętym Ciałem,  
Chwalmy Go na wieki!*

[_metadata_:partType]:- "STROPHE"
**5.**  On, który Swą moc objawia,  
Gdy wyniosłość serc uniża,  
Każdy zamiar może zburzyć,  
Który pychą Mu ubliża

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chrystus Pan karmi nas  
Swoim świętym Ciałem,  
Chwalmy Go na wieki!*

[_metadata_:partType]:- "STROPHE"
**6.**  W mocy Jego odjąć władzę   
a wydźwignąć pokornego  
Wszystkich głodnych zaspokoić   
głodem wstrząsnąć bogatego

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chrystus Pan karmi nas  
Swoim świętym Ciałem,  
Chwalmy Go na wieki!*

[_metadata_:partType]:- "STROPHE"
**7.**  On się ujął za swym ludem   
dziećmi wiary Abrahama  
Pomny na swe miłosierdzie   
obietnicy swej nie złamał


