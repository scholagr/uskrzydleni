[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Kiedyś wino i chleb  
Teraz Ciało i Krew  
Możesz wierzyć lub nie  
To naprawdę dzieje się*

[_metadata_:partType]:- "STROPHE"
**1.**  Ciągle czekasz na cud   
Niespokojny twój duch  
A ja przypomnę, że  
W ciszy i przy blasku świec   
Cud największy dzieje się

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Kiedyś wino i chleb  
Teraz Ciało i Krew  
Możesz wierzyć lub nie  
To naprawdę dzieje się*

[_metadata_:partType]:- "STROPHE"
**2.**  Wypatrujesz co dnia  
Czekasz na jakiś znak  
A ja przypomnę, że  
W ciszy i przy blasku świec  
Cud największy dzieje się


