[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Podnieś mnie Jezu i prowadź do Ojca   
Podnieś mnie Jezu i prowadź do Ojca   
Zanurz mnie w wodzie Jego miłosierdzia   
Amen Amen*

[_metadata_:partType]:- "STROPHE"
**1.**  Pan jest moim Pasterzem   
Niczego mi nie braknie  
Pozwala mi leżeć   
Na zielonych pastwiskach

[_metadata_:partType]:- "STROPHE"
**2.**  Prowadzi mnie nad wody,  
gdzie odpocząć mogę,  
orzeźwia moją duszę

[_metadata_:partType]:- "STROPHE"
**3.**  wiedzie mnie po ścieżkach właściwych,  
przez wzgląd na swoja chwałę,  
Chociaż bym przechodził przez ciemną dolinę,  
zła się nie ulęknę bo Ty jesteś ze mną

[_metadata_:partType]:- "STROPHE"
**4.**  Kij Twój i laska pasterska,  
są moją pociechą,  
stół dla mnie zastawiasz  
na oczach mych wrogów

[_metadata_:partType]:- "STROPHE"
**5.**  Namaszczasz mi głowę,  
olejkiem,  
A kielich mój pełny po   
brzegi

[_metadata_:partType]:- "STROPHE"
**6.**  Dobroć i łaska pójdą w ślad za mną,  
przez wszystkie dni życia,  
i zamieszkam w domu Pana,  
po najdłuższe czasy.


