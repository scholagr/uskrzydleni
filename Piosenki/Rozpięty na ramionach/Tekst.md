[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Rozpięty na ramionach  
Jak sokół na niebie.  
Chrystusie, Synu Boga,  
Spójrz proszę na ziemię.*

[_metadata_:partType]:- "STROPHE"
**1.**  Na ruchliwe ulice,  
Zabieganych ludzi.  
Gdy noc się już kończy,  
A ranek się budzi.  
Uśmiechnij się przyjaźnie   
Z wysokiego krzyża.  
Do ciężko pracujących,  
Cierpiących na pryczach.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Rozpięty na ramionach  
Jak sokół na niebie.  
Chrystusie, Synu Boga,  
Spójrz proszę na ziemię.*

[_metadata_:partType]:- "STROPHE"
**2.**  Pociesz zrozpaczonych,   
chleba daj głodującym.  
Modlących się wysłuchaj   
i wybacz umierającym.  
Spójrz cierpienia sokole   
na wszechświat, na ziemię.  
Na cichy ciemny Kościół,   
dziecko wzywające Ciebie.


