[_metadata_:partType]:- "STROPHE"
**1.**  Jest jedno Ciało, jest jeden Pan  
Jednoczy nas w Duchu, byśmy razem szli.  
Usta głoszą chwałę Mu  
W ręku Słowa Jego miecz,  
 Tak idziemy w moc odziani,  
Zdobywając ziemię tę

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Jesteśmy ludem Króla Chwał  
Jego świętym narodem  
Wybranym pokoleniem,  
By objawiać Jego Cześć  
Jesteśmy ludem Króla Chwał  
Jego świętym narodem  
Wielbimy Jezusa,  
On jest Panem całej ziemi tej.*


