[_metadata_:partType]:- "STROPHE"
**1.**  Głos wdzięczny z nieba wychodzi,  
Gwiazdę k`nam nową wywodzi,  
Która rozświeca ciemności,  
I odkrywa nasze złości.

[_metadata_:partType]:- "STROPHE"
**2.**  Z różdżki Jesse kwiat zakwita,  
który zbawieniem świat wita;  
Pan Bóg zesłał Syna swego,  
przed wieki narodzonego.

[_metadata_:partType]:- "STROPHE"
**3.**  Ojcowie tego czekali,  
tego Prorocy żądali,  
Tego Bóg światu miał zjawić,  
od śmierci człeka wybawić.

[_metadata_:partType]:- "STROPHE"
**4.**  Którego, aby wąż zdradził,  
z rajskich rozkoszy wysadził,  
skusił, by z drzewa rajskiego  
skosztował zakazanego.

[_metadata_:partType]:- "STROPHE"
**5.**  Przez co był z raju wygnany,  
i na wieczną śmierć skazany  
lecz Pan użalił się tego,  
myślał o zbawieniu jego.

[_metadata_:partType]:- "STROPHE"
**6.**  Wnet Anioł Pannie zwiastował,  
o czym Prorok prorokował;  
i miał powstać Syn z zacnego  
Plemienia Dawidowego. Amen.

[_metadata_:partType]:- "STROPHE"
**7.**  Weselcie się ziemskie strony!  
Opuściwszy niebios trony,  
Bóg idzie na te niskości,  
Z niewymownej swej litości

[_metadata_:partType]:- "STROPHE"
**8.**  Weselcie się wszyscy święci!  
I wy, ludzie smutkiem zdjęci;  
Idzie na świat Odkupiciel,  
Strapionych wszystkich Zbawiciel


