[_metadata_:partType]:- "STROPHE"
**1.**  Nie chcę zbyt wysoko sięgać, Panie  
Patrzeć tam, gdzie pycha lśni i chwała  
Chociaż tak wyniosłe ich mieszkanie  
Choć zazdrości godny świata pałac

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Oczekuję Ciebie, Panie  
Jesteś mym oczekiwaniem  
Uspokajasz serce moje  
Uciszeniem jesteś i pokojem mym*

[_metadata_:partType]:- "STROPHE"
**2.**  Nie chcę, by się wywyższało serce  
Wszystko czego pragnę to Twe drogi  
W ciszy na Twym progu siedzieć będę  
Cieszyć się, że jesteś moim Bogiem

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Oczekuję Ciebie, Panie  
Jesteś mym oczekiwaniem  
Uspokajasz serce moje  
Uciszeniem jesteś i pokojem mym*


