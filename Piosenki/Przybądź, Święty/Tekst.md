[_metadata_:partType]:- "STROPHE"
**1.**  Języku ognia przyjdź i płoń w naszych sercach  
Duchu Miłości przemieniaj nas  
Swe pocieszenie nam daj i nim wypełniaj  
Duchu Nadziei umacniaj nas

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Przybądź Święty Niepojęty, z Nieba na nas tchnięty Duchu  
Rozpal Świętą, Niepojętą, z Nieba na nas tchniętą miłość  
Nią ogarnij nas, Amen  
Nią ogarnij nas, Amen*

[_metadata_:partType]:- "STROPHE"
**2.**  Obłoku jasny ochroń nas przed gniewem złego  
Daj soli smak i światłu blask  
Nie daj się ukryć miastu Boga przedwiecznego  
Osłaniaj je, strzeż jego bram.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Przybądź Święty Niepojęty, z Nieba na nas tchnięty Duchu  
Rozpal Świętą, Niepojętą, z Nieba na nas tchniętą miłość  
Nią ogarnij nas, Amen  
Nią ogarnij nas, Amen*

[_metadata_:partType]:- "STROPHE"
**3.**  Pocieszycielu radość Swą ześlij z Nieba  
Wszak Dobry ją obiecał nam  
Przerwij milczenie w Imię Syna słowa chleba  
Najświętsze tchnienie wołaj w nas

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Przybądź Święty Niepojęty, z Nieba na nas tchnięty Duchu  
Rozpal Świętą, Niepojętą, z Nieba na nas tchniętą miłość  
Nią ogarnij nas, Amen  
Nią ogarnij nas, Amen*

[_metadata_:partType]:- "BRIDGE"
**Bridge**  

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Witaj Święty Niepojęty, z Nieba na nas tchnięty Duchu  
Rozpal Świętą, Niepojętą, z Nieba na nas tchniętą miłość  
Nią ogarnij nas, Amen  
Nią ogarnij nas, Amen*


