[_metadata_:partType]:- "STROPHE"
**1.**  Gdy drogi pomyli los zły   
i oczy mgłą zasnuje  
miej w sobie ufność, nie lękaj się.   
  
A kiedy gniew świat ci przesłoni  
i zazdrość jak chwast zakiełkuje.  
Miej w sobie tę ufność, nie lękaj się.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Ty tylko mnie poprowadź,  
Tobie powierzam mą drogę.   
Ty tylko mnie poprowadź, Panie  
  
Ty tylko mnie poprowadź,  
Tobie powierzam mą drogę.   
Ty tylko mnie poprowadź, Panie*

[_metadata_:partType]:- "STROPHE"
**2.**  Poprowadź jak jego prowadzisz,  
przez drogi najprostsze z możliwych  
i pokaż mi jedną, tę jedną z nich.  
  
A kiedy już głos Twój usłyszę  
i karmić się będę nim co dzień,  
Miej w sobie tę ufność, nie lękaj się.


