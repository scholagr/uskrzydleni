[_metadata_:partType]:- "STROPHE"
**1.**  Do naszych serc do wszystkich serc uśpionych  
Dziś zabrzmiał dzwon ,już człowiek obudzony.  
Bo nadszedł czas i dziecię się zrodziło.  
A razem z Nim, maleńka przyszła miłość.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Maleńka miłość w żłobie śpi,  
Maleńka Miłość przy Matce Świętej.  
Dziś cala ziemia i niebo lśni  
dla tej miłości maleńkiej.*

[_metadata_:partType]:- "STROPHE"
**2.**  Porzućmy zło przestańmy złem się bawić.  
I czystą łzą spróbujmy serce zbawić.  
Już nadszedł czas już dziecię się zrodziło.  
A razem z Nim, maleńka nasza miłość.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Maleńka miłość w żłobie śpi,  
Maleńka Miłość przy Matce Świętej.  
Dziś cala ziemia i niebo lśni  
dla tej miłości maleńkiej.*


