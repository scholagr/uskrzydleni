[_metadata_:partType]:- "STROPHE"
**1.**  Patrzysz na mnie takim wzrokiem,   
że z tego wszystkiego aż płakać mi sie chce.  
Patrzysz na mnie takim wzrokiem,  
że bezczelnie proszę o jeszcze.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *W Twoim spojrzeniu słońca,   
w Twoim tchnieniu wiatru,  
w Twojej tęsknocie deszczu,  
czuję miłość twoją Panie.  
W moim smutku i radości,  
w moim zwątpieniu i pewności,  
czuję, czuję miłość Twoją   
Jahwe.*

[_metadata_:partType]:- "STROPHE"
**2.**  Patrzysz na mnie z taką wiarą,  
z jaką nigdy nikt nie odważył spojrzeć się.  
Patrzysz na mnie, choć nie mogę odwzajemnić  
spojrzenia Twego, bo boję się chociaż wiem, że...

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *W Twoim spojrzeniu słońca,   
w Twoim tchnieniu wiatru,  
w Twojej tęsknocie deszczu,  
czuję miłość twoją Panie.  
W moim smutku i radości,  
w moim zwątpieniu i pewności,  
czuję, czuję miłość Twoją   
Jahwe.*

[_metadata_:partType]:- "STROPHE"
**3.**  Panie co oznacza to, czym mnie doświadczasz.  
Twoje spojrzenie jest dla mnie oczyszczeniem.  
Zapraszam Cię Panie do swojego życia.  
Złap mnie Panie za rękę  
i porwij w otchłań Twojej miłości.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *W Twoim spojrzeniu słońca,   
w Twoim tchnieniu wiatru,  
w Twojej tęsknocie deszczu,  
czuję miłość twoją Panie.  
W moim smutku i radości,  
w moim zwątpieniu i pewności,  
czuję, czuję miłość Twoją   
Jahwe.*


