[_metadata_:partType]:- "STROPHE"
**1.**  Niech błyśnie pierwsza gwiazda  
By wytęskniony nastał czas  
Gdy wszystko będzie jasne i światła jakby więcej w nas  
Niech pierwsza gwiazda błyśnie  
By do spotkania wszystkim nam powód dać  
Byś wrócić chciał do domu  
Na małą chwilę dzieckiem się stać

[_metadata_:partType]:- "STROPHE"
**2.**  Do mamy chcesz i taty  
Lecz dzisiaj to już światy dwa  
Dlaczego droga prosta zakrętów tyle ostrych ma  
Dziś drogą nawet krętą  
Niech wróci do nas ciepło dziecięcych lat  
Zwyczajów pełen pięknych nadzwyczajnie piękny świat

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chwała na wysokości  
Dziecięcy słychać śpiew  
Niebiański pokój płynie do naszych serc  
Dzieckiem stać się na nowo  
Niezwykła poczuć moc  
Odnaleźć naszą gwiazdę  
W tę świętą noc*

[_metadata_:partType]:- "STROPHE"
**3.**  Gdy błyśnie gwiazda pierwsza  
Przygasną setki wtórnych gwiazd  
Ten krzyk cokolwiek śmieszny  
I jazgot ulic wielkich miast  
Niech błyśnie pierwsza gwiazda  
By to co ważne, ważne mogło się stać  
Niech błyśnie pierwsza gwiazda  
Niech błyśnie nam jeszcze raz

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chwała na wysokości  
Dziecięcy słychać śpiew  
Niebiański pokój płynie do naszych serc  
Dzieckiem stać się na nowo  
Niezwykła poczuć moc  
Odnaleźć naszą gwiazdę  
W tę świętą noc*

[_metadata_:partType]:- "BRIDGE"
**Bridge**  Jak na imię ma pierwsza gwiazda?  
Kto dziś odnajdzie ją?  
Kto ją zna?

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chwała na wysokości  
Dziecięcy słychać śpiew  
Niebiański pokój płynie do naszych serc  
Dzieckiem stać się na nowo  
Niezwykła poczuć moc  
Odnaleźć naszą gwiazdę  
W tę świętą noc*


