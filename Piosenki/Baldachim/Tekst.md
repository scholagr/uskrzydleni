[_metadata_:partType]:- "STROPHE"
**1.**  Nade mną jest baldachim gwiazd,  
A między nami Twoja twarz.  
Podnoszę głowę i widzę Ciebie tam,   
Ciebie Boże.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *O, jakże chciałbym być Panie z Tobą.  
O, jakże chciałbym być Panie tam.  
I widzieć słońce gdzieś ponad głową, wśród gwiazd  
I nieskończoną dal.*

[_metadata_:partType]:- "BRIDGE"
**Bridge**  Aaaaa.../ 4x

[_metadata_:partType]:- "STROPHE"
**2.**  O Tobie myślą biegnę w dal  
I odnajduję życia smak.  
Jakże ogarnąć Twoją miłość mam?   
Powiedz Panie!


