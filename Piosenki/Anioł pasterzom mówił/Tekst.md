[_metadata_:partType]:- "STROPHE"
**1.**  Anioł pasterzom mówił:   
Chrystus się wam narodził   
w Betlejem, nie bardzo podłym mieście.   
Narodził się w ubóstwie   
Pan waszego stworzenia.

[_metadata_:partType]:- "STROPHE"
**2.**  Chcąc się dowiedzieć tego   
poselstwa wesołego,   
bieżeli do Betlejem skwapliwie.   
Znaleźli dziecię w żłobie,   
Maryję z Józefem.

[_metadata_:partType]:- "STROPHE"
**3.**  Taki Pan chwały wielkiej,   
uniżył się Wysoki,   
pałacu kosztownego żadnego   
nie miał zbudowanego   
Pan waszego stworzenia.

[_metadata_:partType]:- "STROPHE"
**4.**  O dziwne narodzenie,   
nigdy niewysławione !   
Poczęła Panna Syna w czystości,   
porodziła w całości   
Panieństwa swojego

[_metadata_:partType]:- "STROPHE"
**5.**  Już się ono spełniło,   
co pod figurą było:   
Arona różdżka ona zielona   
stała się nam kwitnąca   
i owoc rodząca.

[_metadata_:partType]:- "STROPHE"
**6.**  Słuchajcież Boga Ojca,   
jako wam Go zaleca:   
Ten ci jest Syn najmilszy, jedyny,   
w raju wam obiecany,   
Tego wy słuchajcie.

[_metadata_:partType]:- "STROPHE"
**7.**  Bogu bądź cześć i chwała,   
która byś nie ustała,   
jako Ojcu, tak i Jego Synowi   
i Świętemu Duchowi,   
w Trójcy jedynemu.


