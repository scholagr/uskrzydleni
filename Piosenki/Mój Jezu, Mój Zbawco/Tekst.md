[_metadata_:partType]:- "STROPHE"
**1.**  Mój Jezu, mój Zbawco  
Któż jest tak wielki jak Ty  
Przez wszystkie dni  
Wysławiać chcę  
Wspaniałe dzieła Twoich rąk

[_metadata_:partType]:- "STROPHE"
**2.**  Mój Panie, obrońco  
Źródło mych natchnień i sił  
Niech cały świat  
Wszystko co jest  
Zawsze wielbi Imię Twe!

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Krzycz na cześć Pana  
Rozraduj się w Nim  
Ogłaszaj wszędzie, że on Panem jest  
Góry ustąpią na dźwięk Jego słów  
Gdy przemówi Stwórca ziem!*

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Patrzę z podziwem na dzieła Twych rąk  
Zawsze chcę kochać Cię  
Przy Tobie być  
Co może równać się z tym  
Co u ciebie mam*


