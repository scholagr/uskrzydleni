[_metadata_:partType]:- "STROPHE"
**1.**  capo. 1  
G &#124; G &#124; D &#124; D  
e &#124; e/D &#124; C &#124; C  
G &#124; C &#124; G &#124; C  
F &#124; F &#124; D &#124; D

[_metadata_:partType]:- "STROPHE"
**2.**  G &#124; G &#124; D &#124; D  
e &#124; e/D &#124; C &#124; C  
G &#124; C &#124; G &#124; C  
F &#124; F &#124; D &#124; D

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *G &#124; e &#124; C &#124; D  
G &#124; e &#124; C &#124; D  
e &#124; e &#124; C &#124; C  
D &#124; C &#124; D &#124; D7*

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *G &#124; e &#124; C &#124; D  
G &#124; e &#124; C &#124; D  
e &#124; e &#124; C &#124; D  
G &#124; G &#124; D &#124; D*


