[_metadata_:partType]:- "STROPHE"
**1.**  Przybieżeli do Betlejem pasterze,  
Grając skocznie Dzieciąteczku na lirze.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chwała na wysokości, chwała na wysokości,  
A pokój na ziemi.*

[_metadata_:partType]:- "STROPHE"
**2.**  Oddawali swe ukłony w pokorze  
Tobie z serca ochotnego, o Boże!

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chwała na wysokości, chwała na wysokości,  
A pokój na ziemi.*

[_metadata_:partType]:- "STROPHE"
**3.**  Anioł Pański sam ogłosił te dziwy,  
Których oni nie słyszeli, jak żywi.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chwała na wysokości, chwała na wysokości,  
A pokój na ziemi.*

[_metadata_:partType]:- "STROPHE"
**4.**  Dziwili się napowietrznej muzyce  
i myśleli, co to będzie za Dziecię?

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chwała na wysokości, chwała na wysokości,  
A pokój na ziemi.*

[_metadata_:partType]:- "STROPHE"
**5.**  Oto mu się wół i osioł kłaniają,  
Trzej królowie podarunki oddają.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chwała na wysokości, chwała na wysokości,  
A pokój na ziemi.*

[_metadata_:partType]:- "STROPHE"
**6.**  I anieli gromadą pilnują  
Panna czysta wraz z Józefem pilnują

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chwała na wysokości, chwała na wysokości,  
A pokój na ziemi.*

[_metadata_:partType]:- "STROPHE"
**7.**  Poznali Go Mesjaszem być prawym  
Narodzonym dzisiaj Panem łaskawym

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chwała na wysokości, chwała na wysokości,  
A pokój na ziemi.*

[_metadata_:partType]:- "STROPHE"
**8.**  My go także Bogiem, Zbawcą już znamy  
I z całego serca wszystko kochamy

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chwała na wysokości, chwała na wysokości,  
A pokój na ziemi.*


