[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Szukałem was  
Teraz wy   
do mnie przychodzicie*

[_metadata_:partType]:- "STROPHE"
**1.**  Świadku Chrystusa - módl się za nami  
Patronie małżeństw  
Orędowniku pokoju  
Świadku nadziei  
Przyjacielu młodych  
Drogowskazie życia

[_metadata_:partType]:- "REFRAIN"
***Ref.***  **

[_metadata_:partType]:- "STROPHE"
**2.**  Rybaku ludzi - módl się za nami  
Nadziejo zagubionych  
Niestrudzony pielgrzymie  
Wzorze ojcostwa  
Siewco radości  
Wierny Synu Maryi

[_metadata_:partType]:- "REFRAIN"
***Ref.***  **

[_metadata_:partType]:- "STROPHE"
**3.**  Apostole jedności - módl się za nami  
Towarzyszu cierpiących  
Apostole Miłosierdzia Bożego  
Obrońco życia  
Apostole miłości  
Ty, który czekasz na nas

[_metadata_:partType]:- "REFRAIN"
***Ref.***  **


