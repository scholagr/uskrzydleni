[_metadata_:partType]:- "INTRO"
**Wej.**  

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Skosztujcie i zobaczcie jak dobry jest Pan.*

[_metadata_:partType]:- "STROPHE"
**1.**  Będę Panu w każdej porze  
Śpiewał pieśń wdzięczności,   
Na mych ustach chwała Jego   
Nieustannie gości

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Skosztujcie i zobaczcie jak dobry jest Pan.*

[_metadata_:partType]:- "STROPHE"
**2.**  W Panu cała chluba moja,  
Cieszcie się, pokorni!  
Wspólnie ze mną chwalcie Pana,  
Sławmy Imię Jego!

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Skosztujcie i zobaczcie jak dobry jest Pan.*

[_metadata_:partType]:- "STROPHE"
**3.**  Kiedym tęsknie szukał Pana,  
Raczył mnie wysłuchać  
I od wszelkiej trwogi mojej  
Raczył mnie uwolnić

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Skosztujcie i zobaczcie jak dobry jest Pan.*

[_metadata_:partType]:- "STROPHE"
**4.**  Z czcią i lękiem służcie Panu,   
Święty Ludu Boży.   
Bo nie znają niedostatku   
ludzie bogobojni


