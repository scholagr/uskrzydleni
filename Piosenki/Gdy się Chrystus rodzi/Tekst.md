[_metadata_:partType]:- "STROPHE"
**1.**  Gdy się Chrystus rodzi,   
i na świat przychodzi.   
Ciemna noc w jasności   
promienistej brodzi

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Aniołowie się radują,   
Pod niebiosy wyśpiewują:   
Gloria, gloria, gloria,  
in excelsis Deo!*

[_metadata_:partType]:- "STROPHE"
**2.**  Mówią do pasterzy,  
którzy trzód swych strzegli.   
Aby do Betlejem,  
czym prędzej pobiegli.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Bo się narodził Zbawiciel,   
Wszego świata Odkupiciel.  
Gloria, gloria, gloria,  
in excelsis Deo!*

[_metadata_:partType]:- "STROPHE"
**3.**  "O niebieskie Duchy,   
i posłowie nieba.   
Powiedzcież wyraźniej   
co nam czynić trzeba:

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Bo my nic nie pojmujemy,   
Ledwo od strachu żyjemy".   
Gloria, gloria, gloria,  
in excelsis Deo!*

[_metadata_:partType]:- "STROPHE"
**4.**  "Idźcież do Betlejem,   
gdzie Dziecię zrodzone,   
W pieluszki powite,   
w żłobie położone:

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Oddajcie Mu pokłon boski,   
On osłodzi wasze troski".   
Gloria, gloria, gloria,  
in excelsis Deo!*

[_metadata_:partType]:- "STROPHE"
**5.**  A gdy pastuszkowie   
wszystko zrozumieli  
Zaraz do Betlejem  
śpieszno pobieżeli

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *I zupełnie tak zastali  
Jak anieli im zeznali.  
Gloria, gloria, gloria,  
in excelsis Deo!*

[_metadata_:partType]:- "STROPHE"
**6.**  A stanąwszy na miejscu  
pełni zdumienia  
Iż się Bóg tak zniżył  
do swego stworzenia

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Padli przed Nim na kolana  
I uczcili swego Pana.  
Gloria, gloria, gloria,  
in excelsis Deo!*

[_metadata_:partType]:- "STROPHE"
**7.**  Wreszcie kiedy pokłon  
Panu już oddali  
Z wielką wesołością  
do swych trzód wracali

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Że się stali być godnymi  
Boga widzieć na tej ziemi.  
Gloria, gloria, gloria,  
in excelsis Deo!*


