[_metadata_:partType]:- "STROPHE"
**1.**  W oddali zasypia miasto  
zwyczajny się kończy dzień  
lecz ty nie potrafisz zasnąć  
i nagle pojawia się  
 nad głową tyle gwiazd  
widzisz anioła twarz  
i słyszysz muzykę nieba  
znów oczy przecierasz, to nie sen

[_metadata_:partType]:- "STROPHE"
**2.**  To noc wielkich cudów pełna  
tak z bliska oglądasz je  
i myślisz dlaczego szczęście spotyka dziś właśnie mnie  
nad głowa tyle gwiazd  
widzę anioła twarz  
i słyszę muzykę nieba  
to znak obietnica spełnia się

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Gloria  
w tę niezwykłą noc jest jaśniej niż za dnia  
całe niebo płonie  
Gloria  
niech melodia nieba w każdym sercu gra  
i rozpala ogień  
o, Gloria*

[_metadata_:partType]:- "STROPHE"
**3.**  Gdy rodzi się nowe życie  
co stary odmienia świat  
Przyroda wstrzymuje oddech  
by dziecka usłyszeć płacz

[_metadata_:partType]:- "STROPHE"
**4.**  Prawie nie widać gwiazd  
pełnie anioła twarz  
tu cała muzyka nieba  
w tym małym istnieniu wielki blask!

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Gloria  
w tę niezwykłą noc jest jaśniej niż za dnia  
całe niebo płonie  
Gloria  
niech melodia nieba w każdym sercu gra  
i rozpala ogień  
o, Gloria*

[_metadata_:partType]:- "OUTRO"
**Wyj.**  Świętości świąt na każdy dzień  
miłości więcej, szarości mniej  
Na każdy dzień świętości świąt  
nowego życia na Nowy Rok!  
Świętości świąt (Świętości świąt) na każdy dzień (na każdy dzień)  
miłości więcej (miłości więcej), szarości mniej  
Na każdy dzień (na każdy dzień) świętości świąt (świętości świąt)  
nowego życia (nowego życia) na Nowy Rok!  
O, Gloria!


