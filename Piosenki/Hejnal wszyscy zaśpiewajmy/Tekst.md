[_metadata_:partType]:- "STROPHE"
**1.**  Hejnał wszyscy zaśpiewajmy,  
Cześć i chwałę Panu dajmy,  
Nabożnie k`Niemu wołajmy.

[_metadata_:partType]:- "STROPHE"
**2.**  Mocny Boże z wysokości,  
Ty światłem swej wszechmocności  
Rozpędź piekielne ciemności.

[_metadata_:partType]:- "STROPHE"
**3.**  Jużci ona noc minęła,  
Co wszystek świat ucisnęła,  
A początek z grzechu wzięła.

[_metadata_:partType]:- "STROPHE"
**4.**  Na to Boży Syn jedyny,  
By ciemności zniósł i winy,  
W żywot wstąpił świętej Panny.

[_metadata_:partType]:- "STROPHE"
**5.**  Temu Bóg dał w nas opiekę,  
By czartowską zniósł z nas rękę  
I piekielną odjął mękę

[_metadata_:partType]:- "STROPHE"
**6.**  Ten łaskawie nas przyjmuje,  
Z wiecznych ciemności wyjmuje,  
Światłość zbawienną gotuje.


