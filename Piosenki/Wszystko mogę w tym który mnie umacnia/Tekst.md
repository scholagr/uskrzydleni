[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Wszystko mogę w tym który mnie umacnia   
Jezus umacnia mnie*

[_metadata_:partType]:- "STROPHE"
**1.**  I jestem pewien, że ani śmierć, ani życie,   
ani rzeczy teraźniejsze, ani przyszłe,   
ani co wysokie, ani co głębokie,   
ani jakiekolwiek inne stworzenie nie zdoła mnie odłączyć   
od miłości Boga!  
W Chrystusie Jezusie Panu moim,   
Panu naszym!


