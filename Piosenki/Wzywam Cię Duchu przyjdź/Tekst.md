[_metadata_:partType]:- "STROPHE"
**1.**  Wzywam Cię  
Duchu, przyjdź,   
Czekam wciąż  
byś dotknął nas.

[_metadata_:partType]:- "STROPHE"
**2.**  Wołam Cię  
Panie, przyjdź,   
Jezu, Zbawco  
do dzieci Twych.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Jak spragniona ziemia rosy   
dusza ma.  
Tylko Ty możesz wypełnić   
serca głód,   
serca głód.*

[_metadata_:partType]:- "STROPHE"
**3.**  Głębio mórz,   
potęgo gór.   
Boże mój,  
Nie mogę bez Twej miłości żyć,  
Nie chcę bez Ciebie żyć.


