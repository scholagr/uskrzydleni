[_metadata_:partType]:- "STROPHE"
**1.**  D &#124; D &#124; G &#124; G  
D &#124; D &#124; A &#124; A  
G &#124; G &#124; D &#124; G  
D &#124; A &#124; D &#124; D

[_metadata_:partType]:- "STROPHE"
**2.**  D &#124; D &#124; G &#124; G  
D &#124; D &#124; A &#124; A  
G &#124; G &#124; D &#124; G  
D &#124; A &#124; D &#124; D \ A

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *D &#124; A &#124; G &#124; G  
D &#124; A &#124; G &#124; G  
D &#124; Fis &#124; h &#124; G  
D &#124; A &#124; D &#124; D*

[_metadata_:partType]:- "STROPHE"
**3.**  D &#124; D &#124; G &#124; G  
D &#124; D &#124; A &#124; A  
G &#124; G &#124; D &#124; G  
D &#124; A &#124; D &#124; D \ A

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *D &#124; A &#124; G &#124; G  
D &#124; A &#124; G &#124; G  
D &#124; Fis &#124; h &#124; G  
D &#124; A &#124; D &#124; D*

[_metadata_:partType]:- "BRIDGE"
**Bridge**  A &#124; A &#124; D &#124; D  
A &#124; A &#124; D &#124; D  
fis &#124; h &#124; fis &#124; h  
G &#124; G &#124; A &#124; A &#124; A

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *D &#124; A &#124; G &#124; G  
D &#124; A &#124; G &#124; G  
D &#124; Fis &#124; h &#124; G  
D &#124; A &#124; D &#124; D*


