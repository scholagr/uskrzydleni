[_metadata_:partType]:- "STROPHE"
**1.**  Ten szczególny dzień się budzi,  
niosąc ciepło w każdą sień.  
To dobroci dzień dla ludzi,  
tylko jeden w całym roku taki dzień.

[_metadata_:partType]:- "STROPHE"
**2.**  Zmierzchem błyśnie nam promienna,  
gasząc w sercach naszych złość,  
i nadejdzie noc pojednań,  
tylko jedna w całym roku taka noc.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Choć tyle żalu w nas,  
I gniew uśpiony trwa  
Przekażcie sobie znak pokoju,  
Przekażcie sobie znak.*

[_metadata_:partType]:- "STROPHE"
**3.**  Potem przyjdą dni powszednie,  
braknie nagle ciepłych słów,  
najjaśniejsza gwiazda zblednie  
i niepokój, jak co roku, wróci znów.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Choć tyle żalu w nas,  
I gniew uśpiony trwa  
Przekażcie sobie znak pokoju,  
Przekażcie sobie znak.*

[_metadata_:partType]:- "BRIDGE"
**Bridge**  Niejeden świt powróci zwątpień mgłą,   
brzemieniem spraw i trosk.  
Powróci twarzy mars na powitanie dnia,  
znów milczenie serc regułą będzie nam.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Choć tyle żalu w nas,  
I gniew uśpiony trwa  
Przekażcie sobie znak pokoju,  
Przekażcie sobie znak.*


