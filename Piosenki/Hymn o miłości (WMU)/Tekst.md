[_metadata_:partType]:- "STROPHE"
**1.**  Gdybym mówił językami ludzi i aniołów,  
A miłości bym nie miał.  
Stałbym się jak miedź brzęcząca  
Albo cymbał brzmiący.  
Gdybym też miał dar prorokowania i poznał  
wszystkie tajemnice.  
I posiadł wszelką wiedzę  
I wszelką wiarę, tak iżbym góry przenosił.  
A miłości bym nie miał, byłbym niczym.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Połóż mnie jak pieczęć na twoim sercu,  
jak pieczęć na twoim ramieniu  
Bo jak śmierć potężna jest miłość,  
a zazdrość jej nieprzejednana  
Żar jej to żar ognia, płomień Pański  
Wody wielkie nie zdołają jej ugasić  
Nie zatopią jej rzeki  
Jeśli ktoś ją odda za srebrniki, pogardzą nim tylko*

[_metadata_:partType]:- "STROPHE"
**2.**  I gdybym rozdał na jałmużnę całą majętność moją,  
A ciało wystawił na spalenie,  
Lecz miłości bym nie miał, nic bym nie zyskał.  
Miłość cierpliwa jest, łaskawa jest, miłość nie zazdrości  
Nie szuka poklasku, nie unosi się pychą,  
nie dopuszcza się bezwstydu  
Nie szuka swego, nie unosi się gniewem, nie pamięta złego

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Połóż mnie jak pieczęć na twoim sercu,  
jak pieczęć na twoim ramieniu  
Bo jak śmierć potężna jest miłość,  
a zazdrość jej nieprzejednana  
Żar jej to żar ognia, płomień Pański  
Wody wielkie nie zdołają jej ugasić  
Nie zatopią jej rzeki  
Jeśli ktoś ją odda za srebrniki, pogardzą nim tylko*

[_metadata_:partType]:- "STROPHE"
**3.**  Nie cieszy się z niesprawiedliwości, lecz współweseli się  
z prawdą  
Wszystko znosi, wszystkiemu wierzy,  
We wszystkim pokłada nadzieję, wszystko przetrzyma.  
Miłość nigdy nie ustaje, nie jest jak proroctwa,  
które się skończą  
Albo jak dar języków, który zniknie, lub jak wiedza,  
której zabraknie.  
Tak i więc trwają: wiara, nadzieja i miłość - te trzy  
Z nich zaś największa jest miłość.


