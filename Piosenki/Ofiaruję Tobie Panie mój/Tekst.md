[_metadata_:partType]:- "STROPHE"
**1.**  Ofiaruję Tobie Panie mój  
całe życie me  
cały jestem twój - aż na wieki.  
Oto moje serce, przecież wiesz  
Tyś miłością mą jedyną jest.

[_metadata_:partType]:- "STROPHE"
**2.**  Przed ołtarzem świętym niosę Ci  
Pokarm duszy mej.  
Ty zamieniasz go w Krew i Ciało  
Oto moje serce przecież w wiesz  
Tyś miłością mą jedyną jest!

[_metadata_:partType]:- "STROPHE"
**3.**  Ty w ofierze Swojej Panie mój  
cały dajesz się  
cały jesteś mój - aż na wieki.  
Oto moje serce, proszę weź  
Miłość Twoja niech umocni je!

[_metadata_:partType]:- "STROPHE"
**4.**  Ofiaruję Tobie Panie mój  
Wszystkie myśli me  
Tobie ufam wiesz, bardzo mocno  
Bo to moje serce przecież wiesz  
Tyś miłością mą jedyną jest


