[_metadata_:partType]:- "STROPHE"
**1.**  Dobranoc, Głowo święta Jezusa mojego,  
Któraś była zraniona do mózgu samego.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Dobranoc Kwiecie różany,  
Dobranoc Jezu kochany, dobranoc!*

[_metadata_:partType]:- "STROPHE"
**2.**  Dobranoc, włosy święte, mocno potarganę,   
Które były najświętszą Krwią zafarbowane.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Dobranoc Kwiecie różany,  
Dobranoc Jezu kochany, dobranoc!*

[_metadata_:partType]:- "STROPHE"
**3.**  Dobranoc, szyjo święta, w łańcuch uzbrajana,   
Bądź po wszystkie wieczności mile pochwalona.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Dobranoc Kwiecie różany,  
Dobranoc Jezu kochany, dobranoc!*

[_metadata_:partType]:- "STROPHE"
**4.**  Dobranoc ręce święte, na krzyż wyciągnione,   
Jako struny na lutni, gdy są wystrojone.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Dobranoc Kwiecie różany,  
Dobranoc Jezu kochany, dobranoc!*

[_metadata_:partType]:- "STROPHE"
**5.**  Dobranoc, boku święty, z którego płynęła  
Krew najświętsza, by grzechy człowieka obmyła.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Dobranoc Kwiecie różany,  
Dobranoc Jezu kochany, dobranoc!*

[_metadata_:partType]:- "STROPHE"
**6.**  Dobranoc, Serce święte, włócznią otworzone,  
Bądź po wszystkie wieczności mile pozdrowione.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Dobranoc Kwiecie różany,  
Dobranoc Jezu kochany, dobranoc!*

[_metadata_:partType]:- "STROPHE"
**7.**  Dobranoc, nogi święte, na wylot przeszyte,   
I tępymi gwoździami do krzyża przybite.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Dobranoc Kwiecie różany,  
Dobranoc Jezu kochany, dobranoc!*

[_metadata_:partType]:- "STROPHE"
**8.**  Dobranoc, Krzyżu święty, z którego złożony,   
Jezus; w prześcieradło białe uwiniony.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Dobranoc Kwiecie różany,  
Dobranoc Jezu kochany, dobranoc!*

[_metadata_:partType]:- "STROPHE"
**9.**  Dobranoc, grobie święty, najświętszego Ciała,   
Który Matka Bolesna łzami oblewała.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Niech Ci będzie cześć w wieczności,   
Za Twe męki, zelżywości,  
Mój Jezu!*

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Niech Ci będzie cześć i chwała  
Za Twą boleść którąś miała, Maryjo.*


