[_metadata_:partType]:- "STROPHE"
**1.**  Pańska jest ziemia, i co jest na ziemi,  
Jej długi okrąg z mieszkańcami swymi;  
On ją na morzach utrzymuje stale,  
I miękkie wody chciał dać za grunt skale.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Otwórzcie bramy, co nietknione stały  
Bramy wieczyste! Bo idzie Król chwały.  
Któż ten Król chwały? I Kto jest ten mężny?  
Pan mocny w boju, i Bóg nasz potężny.  
  
Otwórzcie bramy, co nietknione stały.  
Otwórzcie bramy, bo idzie Król chwały.  
Któż ten Król chwały? Pan o cnoty dbały.  
On w te drzwi wnidzie, on jest Królem chwały.*

[_metadata_:partType]:- "STROPHE"
**2.**  Któż na twą górę może wstąpić, Panie?  
Albo na miejscu poświęconym stanie?  
Ten, który krzywdą rąk swych nie poszpeci,  
Ten, co ma serce czyste, Bożych dzieci.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Otwórzcie bramy, co nietknione stały  
Bramy wieczyste! Bo idzie Król chwały.  
Któż ten Król chwały? I Kto jest ten mężny?  
Pan mocny w boju, i Bóg nasz potężny.  
  
Otwórzcie bramy, co nietknione stały.  
Otwórzcie bramy, bo idzie Król chwały.  
Któż ten Król chwały? Pan o cnoty dbały.  
On w te drzwi wnidzie, on jest Królem chwały.*

[_metadata_:partType]:- "STROPHE"
**3.**  Kto dba o duszę, nie przysiągł kłamliwie,  
Z pańskiej litości pójdzie mu szczęśliwie.  
Oto jest rodzaj i taka rachuba,  
Tych, co chcą znaleźć twarz Boga Jakuba.


