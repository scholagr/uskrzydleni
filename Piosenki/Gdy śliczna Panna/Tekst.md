[_metadata_:partType]:- "STROPHE"
**1.**  Gdy śliczna Panna   
Syna kołysała,   
z wielkim weselem   
tak Jemu śpiewała:

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Li li li li laj,   
moje Dzieciąteczko,   
li li li li laj,   
śliczne Paniąteczko.*

[_metadata_:partType]:- "STROPHE"
**2.**  Wszystko stworzenie,   
śpiewaj Panu swemu,   
pomóż radości   
wielkiej sercu memu.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Li li li li laj,   
wielki królewiczu,   
li li li li laj,   
niebieski dziedzicu !*

[_metadata_:partType]:- "STROPHE"
**3.**  Sypcie się z nieba,   
śliczni aniołowie,   
śpiewajcie Panu,   
niebiescy duchowie:

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Li li li li laj,   
mój wonny kwiateczku,   
li li li li laj,   
w ubogim żłóbeczku.*

[_metadata_:partType]:- "STROPHE"
**4.**  Cicho wietrzyku,   
cicho południowy.  
Cicho powiewaj,   
niech śpi Panicz nowy.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Li li li li laj,   
wielki królewiczu,   
li li li li laj,   
niebieski dziedzicu !*

[_metadata_:partType]:- "STROPHE"
**5.**  Cicho bydlątka   
parą swą chuchajcie,  
ślicznej Dziecinie   
snu nie przerywajcie

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Li li li li laj,   
wielki królewiczu,   
li li li li laj,   
niebieski dziedzicu !*

[_metadata_:partType]:- "STROPHE"
**6.**  Nic mi nie mówisz,   
o Kochanie moje,  
przecież pojmuję   
w sercu słowa Twoje

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Li li li li laj,   
wielki królewiczu,   
li li li li laj,   
niebieski dziedzicu !*


