[_metadata_:partType]:- "STROPHE"
**1.**  Ty wyzwoliłeś nas Panie  
Z kajdan i samych siebie  
A Chrystus stając się bratem  
Nauczył nas wołać do Ciebie:

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Abba Ojcze!   
Abba Ojcze!  
Abba Abba Ojcze!   
Abba Ojcze!*

[_metadata_:partType]:- "STROPHE"
**2.**  Bo Kościół jak drzewo życia  
W wieczności zapuszcza korzenie  
Przenika naszą codzienność  
I pokazuje nam Ciebie

[_metadata_:partType]:- "REFRAIN"
***Ref.***  **

[_metadata_:partType]:- "STROPHE"
**3.**  Bóg hojnym dawcą jest życia  
On wyswobodził nas z śmierci  
I przygarniając do siebie  
Uczynił swoimi dziećmi.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  **

[_metadata_:partType]:- "STROPHE"
**4.**  Wszyscy jesteśmy braćmi  
Jesteśmy jedną rodziną.  
Tej prawdy nic już nie zaćmi  
I teraz jest jej godzina.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  **


