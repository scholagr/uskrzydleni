[_metadata_:partType]:- "STROPHE"
**1.**  Jezusa narodzonego wszyscy witajmy  
Jemu po kolędzie dary wzajem oddajmy;

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *oddajmy wesoło, skłaniajmy swe czoło,  
skłaniajmy swe czoło Panu naszemu.*

[_metadata_:partType]:- "STROPHE"
**2.**  Oddajmy za złoto wiarę, czyniąc wyznanie,  
że to Bóg i Człowiek prawy leży na sianie;

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *oddajmy wesoło, skłaniajmy swe czoło,  
skłaniajmy swe czoło Panu naszemu.*

[_metadata_:partType]:- "STROPHE"
**3.**  Oddajmy też za kadzidło Panu nadzieję,  
że Go będziem widzieć w niebie, mówiąc to śmiele;

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *oddajmy wesoło, skłaniajmy swe czoło,  
skłaniajmy swe czoło Panu naszemu.*

[_metadata_:partType]:- "STROPHE"
**4.**  Oddajmy za mirę miłość na dowód tego,  
że Go nad wszystko kochamy, z serca całego;

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *oddajmy wesoło, skłaniajmy swe czoło,  
skłaniajmy swe czoło Panu naszemu.*

[_metadata_:partType]:- "STROPHE"
**5.**  Przyjmij, Jezu, na kolędę te nasze dary,  
przyjmij serca, dusze nasze za swe ofiary,

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *byśmy kiedyś w niebie, posiąść mogli Ciebie  
posiąść mogli Ciebie na wieki wieków.*


