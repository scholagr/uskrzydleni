[_metadata_:partType]:- "STROPHE"
**1.**  Krzyżu Chrystusa, bądźże pochwalony,  
Na wieczne czasy bądźże pozdrowiony:  
Gdzie Bóg, Król świata całego   
dokonał życia swojego

[_metadata_:partType]:- "STROPHE"
**2.**  Krzyżu Chrystusa bądźże pochwalony,  
Na wieczne czasy bądźże pozdrowiony:  
Ta sama Krew Cię skropiła,  
Która nas z grzechów obmyła

[_metadata_:partType]:- "STROPHE"
**3.**  Krzyżu Chrystusa bądźże pochwalony,  
Na wieczne czasy bądźże pozdrowiony:  
Z ciebie moc płynie i męstwo,  
W tobie jest nasze zwycięstwo


