[_metadata_:partType]:- "STROPHE"
**1.**  Nic nas nie zdoła odłączyć od Ciebie  
Ani śmierć, ani życie, utrapienie i prześladowanie   
Nic nas nie zdoła odłączyć od Ciebie   
Ale we wszystkim tym odnosimy pełnię zwycięstwa   
Dzięki temu, który nas umiłował

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Zbudowani na Chrystusie,  
w Nim zakorzenieni,  
mocni w wierze i wdzięczni,  
bo On jest źródłem życia.*

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Alleluja  
Alleluja  
Alleluja  
Alleluja*


