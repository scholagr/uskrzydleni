[_metadata_:partType]:- "STROPHE"
**1.**  Krzyżu święty, nade wszystko,  
drzewo przenajszlachetniejsze!  
W żadnym lesie takie nie jest,  
jedno, na którym sam Bóg jest.  
Słodkie drzewo, słodkie gwoździe  
rozkoszny owoc nosiło.

[_metadata_:partType]:- "STROPHE"
**2.**  Skłoń gałązki, drzewo święte,  
Ulżyj członkom tak rozpiętym.  
Odmień teraz oną srogość,  
Którąś miało z urodzenia.  
Spuść lekuchno i cichuchno  
Ciało Króla niebieskiego.

[_metadata_:partType]:- "STROPHE"
**3.**  Tyś samo było dostojne,  
Nosić światowe Zbawienie,  
Przez cię przewóz jest naprawion,  
Światu, który był zagubion,  
Który święta Krew polała,  
Co z Baranka wypływała.

[_metadata_:partType]:- "STROPHE"
**4.**  W jasełkach leżąc gdy płakał,  
Już tam był wszystko oglądał,  
Iż tak haniebnie umrzeć miał,  
Gdy wszystek świat odkupić chciał  
W on czas między zwierzętami  
A teraz między łotrami.

[_metadata_:partType]:- "STROPHE"
**5.**  Niesłychanać to jest dobroć,  
Za kogo na krzyżu umrzeć,  
Któż to może dzisiaj zdziałać,  
Za kogo swoją duszę dać?  
Sam to Pan Jezus wykonał,  
Bo nas wiernie umiłował.

[_metadata_:partType]:- "STROPHE"
**6.**  Nędzne by to serce było,  
co by dziś nie zapłakało,  
widząc Stworzyciela swego  
na krzyżu zawieszonego.  
Na słońcu upieczonego  
Baranka Wielkanocnego.

[_metadata_:partType]:- "STROPHE"
**7.**  Maryja, matka patrzyła  
Na członki, co powijała,  
Powijając całowała,  
Z tego wielką radość miała  
Teraz je widzi sczerniałe,  
Żyły, stawy w Nim porwane.

[_metadata_:partType]:- "STROPHE"
**8.**  Nie był taki, ani będzie  
Żadnemu smutek na świecie,  
Jaki czysta Panna miała  
Wonczas, kiedy narzekała:  
Nędzna ja sierota dzisiaj.  
Do kogóż ja się skłonić mam.

[_metadata_:partType]:- "STROPHE"
**9.**  Jednegom Synaczka miała,  
Com Go z nieba być poznała,  
I tegom już postradała,  
Jenom sama się została,  
Ciężki ból cierpi me serce,  
Od żalu mi się rozpaść chce.

[_metadata_:partType]:- "STROPHE"
**10.**  W radościm Go porodziła,  
Smutku żadnego nie miała,  
A teraz wszystkie, boleści  
Dręczą mnie dziś bez litości;  
Obymże Ja to mogła mieć,  
Żebym mogła teraz umrzeć.

[_metadata_:partType]:- "STROPHE"
**11.**  Byś mi, synu, nisko wisiał,  
Wżdybyś ze mnie pomoc miał,  
Głowę bym Twoją podparła,  
Krew zsiadłą z lica otarła;  
Ale Cię nie mogę dosiąc,  
Tobie, Synu, nic dopomóc

[_metadata_:partType]:- "STROPHE"
**12.**  Anielskie się słowa mienią,  
Symeonowe się pełnią;  
Anioł rzekł: Pełnaś miłości,  
A jam dziś pełna gorzkości.  
Symeon mi to powiedział,  
Iż me serce miecz przebóść miał.

[_metadata_:partType]:- "STROPHE"
**13.**  Ni ja ojca, matki, brata,  
Ni żadnego przyjaciela  
Skądże pocieszenie mam mieć?  
Wolałabym stokroć umrzeć,  
Niżwidzieć żołnierza złego  
Co przebił bok Syna mego.

[_metadata_:partType]:- "STROPHE"
**14.**  Matki, co synaczki macie,  
Jako się w nich wy kochacie,  
Kiedy wam z nich jeden umrze,  
Ciężki ból ma serce wasze;  
Cóż ja, com miała jednego,  
Już nie mogę mieć inszego.

[_metadata_:partType]:- "STROPHE"
**15.**  O, niestetyż, miły Panie,  
Toć nie małe rozłączenie;  
przedtem było miłowanie  
A teraz ciężkie wzdychanie.  
Czemuż, Boże Ojcze nie dbasz,  
O Synaczka pieczy nie masz?

[_metadata_:partType]:- "STROPHE"
**16.**  Którzy tej Pannie służycie,  
Smutki Jej rozmyśliwajcie,  
Jako często omdlewała,  
Często na ziemię padała.  
Przez te smutki, któreś miała,  
Uprośże nam wieczną chwałę!


