[_metadata_:partType]:- "STROPHE"
**1.**  Słuchaj Izraelu, słuchaj moich słów   
słuchaj cała ziemio, co zamierza Bóg.   
Z Maryi Dziewicy, narodzi się Syn,   
Zbawca Świata Odkupiciel, Jezus Chrystus Król!   
Z Maryi Dziewicy, narodzi się Syn,   
Zbawca Świata Odkupiciel, Jezus Chrystus Król!

[_metadata_:partType]:- "STROPHE"
**2.**  Słuchaj Izraelu, słuchaj moich słów  
Z niewoli egipskiej prowadzi cię Bóg  
W ten nieznany jeszcze choć szczęśliwy kraj  
W swoim prawie, w swoim trudzie Izraelu trwaj

[_metadata_:partType]:- "STROPHE"
**3.**  Nad Brzegi Jordanu, przyszedł wielki tłum,   
Słuchać słów Proroka, przyjąć Jego Chrzest.   
Jam niegodny sługa, po mnie przyjdzie On-   
Zbawca świata Odkupiciel, Jezus Chrystus Król!


