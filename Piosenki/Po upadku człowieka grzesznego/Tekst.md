[_metadata_:partType]:- "STROPHE"
**1.**  Po upadku człowieka grzesznego  
Użalił się Pan stworzenia swego,  
Zesłał na świat Archanioła cnego.

[_metadata_:partType]:- "STROPHE"
**2.**  Idź do Panny, imię Jej Maryja,  
spraw poselstwo: Zdrowaś, łaski pełna,  
Pan jest z Tobą, nie bądźże troskliwa".

[_metadata_:partType]:- "STROPHE"
**3.**  Panna natenczas psałterz czytała,  
gdy pozdrowienie to usłyszała,  
na słowa się anielskie zdumiała.

[_metadata_:partType]:- "STROPHE"
**4.**  Archanioł, widząc Pannę troskliwą,  
jął Ją cieszyć mową łagodliwą:  
Panno, nie lękaj się, Pan jest z Tobą!"

[_metadata_:partType]:- "STROPHE"
**5.**  Znalazłaś łaskę u Pana swego  
Ty się masz stać Matką Syna Jego  
Ta jest wola Boga wszechmocnego.


