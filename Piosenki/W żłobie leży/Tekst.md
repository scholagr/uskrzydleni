[_metadata_:partType]:- "STROPHE"
**1.**  W żłobie leży, któż pobieży  
Kolędować małemu  
Jezusowi Chrystusowi  
Dziś nam narodzonemu?

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Pastuszkowie przybywajcie  
Jemu wdzięcznie przygrywajcie  
Jako Panu naszemu!*

[_metadata_:partType]:- "STROPHE"
**2.**  My zaś sami z piosneczkami  
Za wami się śpieszmy,  
I tak tego, maleńkiego,  
Niech wszyscy zobaczmy:

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Jak ubogo narodzony,  
Płacze w stajni położony,  
Więc go dziś ucieszmy.*

[_metadata_:partType]:- "STROPHE"
**3.**  Naprzód tedy, niechaj wszędy  
Zabrzmi świat w wesołości,  
Że posłany nam jest dany  
Emmanuel w niskości!

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Jego tedy przywitajmy,  
Z aniołami zaśpiewajmy  
Chwała na wysokości!*

[_metadata_:partType]:- "STROPHE"
**4.**  Witaj, Panie, cóż się stanie,  
Że rozkosze niebieskie  
Opuściłeś a zstąpiłeś  
Na te niskości ziemskie?

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Miłość moja to sprawiła,  
By człowieka wywyższyła  
Pod nieba emipryjskie.*


