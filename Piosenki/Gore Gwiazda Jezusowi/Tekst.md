[_metadata_:partType]:- "STROPHE"
**1.**  Gore gwiazda Jezusowi w obłoku  
Józef z Panną asystują przy boku, przy boku

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Hejże ino dyna, dyna, narodził się Bóg dziecina.  
W Betlejem, w Betlejem.  
Hejże ino dyna, dyna, narodził się Bóg dziecina  
W Betlejem, w Betlejem*

[_metadata_:partType]:- "STROPHE"
**2.**  Wół i osioł w parze służą przy żłobie, przy żłobie  
Huczą, buczą delikatnej osobie, osobie

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Hejże ino dyna, dyna, narodził się Bóg dziecina.  
W Betlejem, w Betlejem.  
Hejże ino dyna, dyna, narodził się Bóg dziecina  
W Betlejem, w Betlejem*

[_metadata_:partType]:- "STROPHE"
**3.**  Pastuszkowie z podarunki przybiegli, przybiegli  
W koło szopę o północy obiegli, obiegli

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Hejże ino dyna, dyna, narodził się Bóg dziecina.  
W Betlejem, w Betlejem.  
Hejże ino dyna, dyna, narodził się Bóg dziecina  
W Betlejem, w Betlejem*

[_metadata_:partType]:- "STROPHE"
**4.**  Anioł Pański sam ogłosił te dziwy  
Których oni nie słyszeli jak żywi, jak żywi

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Hejże ino dyna, dyna, narodził się Bóg dziecina.  
W Betlejem, w Betlejem.  
Hejże ino dyna, dyna, narodził się Bóg dziecina  
W Betlejem, w Betlejem*

[_metadata_:partType]:- "STROPHE"
**5.**  Anioł Pański kuranciki, kuranciki wycina, wycina  
Stąd pociecha dla człowieka jedyna, jedyna

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Hejże ino dyna, dyna, narodził się Bóg dziecina.  
W Betlejem, w Betlejem.  
Hejże ino dyna, dyna, narodził się Bóg dziecina  
W Betlejem, w Betlejem*

[_metadata_:partType]:- "STROPHE"
**6.**  Już Maryja Jezuleńka powiła, powiła  
Stąd wesele i pociecha nam miła, nam miła

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Hejże ino dyna, dyna, narodził się Bóg dziecina.  
W Betlejem, w Betlejem.  
Hejże ino dyna, dyna, narodził się Bóg dziecina  
W Betlejem, w Betlejem*


