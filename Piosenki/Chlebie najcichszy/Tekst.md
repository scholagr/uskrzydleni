[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chlebie najcichszy,  
otul mnie swym milczeniem,  
ukryj mnie w swojej bieli,  
wchłoń moją ciemność.*

[_metadata_:partType]:- "STROPHE"
**1.**  Przemień mnie w siebie,  
bym jak Ty stał się chlebem. / x2   
Pobłogosław mnie, połam,  
rozdaj łaknącym braciom. / x2

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chlebie najcichszy,  
otul mnie swym milczeniem,  
ukryj mnie w swojej bieli,  
wchłoń moją ciemność.*

[_metadata_:partType]:- "STROPHE"
**2.**  A ułomki chleba,   
które zostaną / x2  
rozdaj tym,   
którzy nie wierzą w swój głód. / x2


