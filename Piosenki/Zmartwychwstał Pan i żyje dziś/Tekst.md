[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Zmartwychwstał Pan i żyje dziś,   
blaskiem jaśnieje noc   
Nie umrę, nie lecz będę żył,   
Pan okazał swą moc  
Krzyż to jest brama Pana,   
jeśli chcesz przez nią wejdź  
Zbliżmy się do ołtarza,   
Bogu oddajmy cześć*

[_metadata_:partType]:- "STROPHE"
**1.**  Dzięki składajmy Mu, bo wielka jest jego łaska   
Z grobu powstał dziś Pan, a noc jest pełna blasku   
Chcę dziękować Mu i chcę Go dziś błogosławić  
Jezus, mój Pan i Bóg, On przyszedł aby nas zbawić

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Zmartwychwstał Pan i żyje dziś,   
blaskiem jaśnieje noc   
Nie umrę, nie lecz będę żył,   
Pan okazał swą moc  
Krzyż to jest brama Pana,   
jeśli chcesz przez nią wejdź  
Zbliżmy się do ołtarza,   
Bogu oddajmy cześć*

[_metadata_:partType]:- "STROPHE"
**2.**  Lepiej się uciec do Pana, niż zaufać książętom  
Pan, moja moc i pieśń, podtrzymał gdy mnie popchnięto  
Już nie będę się bał – cóż może zrobić mi śmierć?  
Nie, nie lękam się i śpiewam chwały pieśń

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Zmartwychwstał Pan i żyje dziś,   
blaskiem jaśnieje noc   
Nie umrę, nie lecz będę żył,   
Pan okazał swą moc  
Krzyż to jest brama Pana,   
jeśli chcesz przez nią wejdź  
Zbliżmy się do ołtarza,   
Bogu oddajmy cześć*

[_metadata_:partType]:- "STROPHE"
**3.**  Odrzucony Pan stał się kamieniem węgielnym  
Pan wysłuchał mnie, On jest zbawieniem mym  
Cudem staje się noc, gdy w dzień jest przemieniona  
Tańczmy dla niego dziś, prawica Pańska wzniesiona


