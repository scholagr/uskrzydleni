[_metadata_:partType]:- "STROPHE"
**1.**  Błogosławcie Pana wszystkie Jego dzieła,  
Które by nie kwitły, gdyby nie cierpienie,  
Błogosławcie Pana.

[_metadata_:partType]:- "STROPHE"
**2.**  Błogosławcie Pana wszystkie łzy i żale,  
Każda moja słabość i upokorzenie,  
Błogosławcie Pana

[_metadata_:partType]:- "STROPHE"
**3.**  Błogosławcie Pana wszystkie moje rany,  
Na które Duch Święty staje się balsamem ,  
Błogosławcie Pana.

[_metadata_:partType]:- "STROPHE"
**4.**  Niech Cię błogosławią wszystkie me niemoce,  
Ból skrzętnie skrywany po bezsennej nocy,  
Niech Cię błogosławią

[_metadata_:partType]:- "STROPHE"
**5.**  Błogosławcie Pana, że Mu ufam jeszcze,  
Że mnie wciąż uzdrawia łaski swojej deszczem,  
Błogosławcie Pana


