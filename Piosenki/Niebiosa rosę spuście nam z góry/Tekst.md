[_metadata_:partType]:- "STROPHE"
**1.**  Niebiosa, rosę spuśćcie nam z góry;  
Sprawiedliwego wylejcie, chmury.  
O wstrzymaj, wstrzymaj Twoje zagniewanie  
I grzechów naszych zapomnij już, Panie!

[_metadata_:partType]:- "STROPHE"
**2.**  Niebiosa, rosę spuście nam z góry;  
Sprawiedliwego wylejcie, chmury.  
Grzech nas oszpecił i w sprośnej postaci  
stoim przed Tobą, jakby trędowaci.

[_metadata_:partType]:- "STROPHE"
**3.**  Niebiosa, rosę spuście nam z góry;  
Sprawiedliwego wylejcie, chmury.  
O, spojrzyj, spojrzyj na lud Twój znękany  
I ześlij Tego, co ma być zesłany.

[_metadata_:partType]:- "STROPHE"
**4.**  Niebiosa, rosę spuście nam z góry;  
Sprawiedliwego wylejcie, chmury.  
Pociesz się, ludu, pociesz w swej niedoli,  
Już się przybliża kres twojej niewoli

[_metadata_:partType]:- "STROPHE"
**5.**  Niebiosa, rosę spuście nam z góry;  
Sprawiedliwego wylejcie, chmury.  
Roztwórz się ziemio, i z łona twojego,  
Wydaj nam, wydaj już Zbawcę naszego.


