[_metadata_:partType]:- "STROPHE"
**1.**  Bóg się rodzi, moc truchleje:   
Pan niebiosów obnażony,  
Ogień krzepnie, blask ciemnieje,   
ma granice Nieskończony;

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Wzgardzony, okryty chwałą,   
śmiertelny Król nad wiekami  
A Słowo Ciałem się stało   
i mieszkało między nami.*

[_metadata_:partType]:- "STROPHE"
**2.**  Cóż masz niebo, nad ziemiany?   
Bóg porzucił szczęście twoje,  
Wszedł między lud ukochany,   
dzieląc z nim trudy i znoje,

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Niemało cierpiał, niemało,   
żeśmy byli winni sami  
A Słowo Ciałem się stało   
i mieszkało między nami.*

[_metadata_:partType]:- "STROPHE"
**3.**  Podnieś rękę Boże Dziecię!   
Błogosław ojczyznę miłą.  
W dobrych radach, w dobrym bycie   
wspieraj jej siłę swą siłą,

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Dom nasz i majętność całą   
i wszystkie wioski z miastami  
A Słowo Ciałem się stało   
i mieszkało między nami*


