[_metadata_:partType]:- "STROPHE"
**1.**  Uwielbiajcie Pana	   
ludzkich serc bijące dzwony  
Padnij na kolana 	   
przed Nim ludu utrudzony

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *On osuszy twoje łzy,	  
On ratunkiem będzie ci*

[_metadata_:partType]:- "STROPHE"
**2.**  Tu, do Jego stóp padnie wróg,	  
Bo On Bóg nie zwyciężony

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Niepojęty w swej mądrości,   
Święty, święty Bóg miłości.*

[_metadata_:partType]:- "STROPHE"
**3.**  Śpiewaj Panu ziemio,  
Chwalcie wszystkie świata strony


