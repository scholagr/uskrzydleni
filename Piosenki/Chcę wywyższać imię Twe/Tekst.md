[_metadata_:partType]:- "STROPHE"
**1.**  Chcę wywyższać Imię Twe,  
chcę zaśpiewać Tobie chwałę,  
Panie, dziś raduję się,  
bo przyszedłeś, by mnie zbawić.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Z nieba zstąpiłeś i chcesz   
prowadzić mnie,  
na krzyżu zmarłeś, by mój   
zapłacić dług,  
z grobu wstałeś i dziś   
nieba Królem jesteś Ty,   
chcę wywyższać Imię Twe.*


