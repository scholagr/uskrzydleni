[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Tchnij moc, tchnij miłość  
i przenikaj życie me.*

[_metadata_:partType]:- "STROPHE"
**1.**  Bo całym sercem swym  
oddaję Ci cześć,  
bo każdą myślą swą oddaję Ci cześć.  
I z całej siły swej oddaję Ci cześć,  
o, Panie mój!

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Tchnij moc, tchnij miłość  
i przenikaj życie me.*

[_metadata_:partType]:- "STROPHE"
**2.**  Bo całym sercem swym uwielbiać Cię chcę.  
Bo każdą myślą swą uwielbiać Cię chcę.  
I z całej siły swej uwielbiać Cię chcę,   
o, Panie mój!

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Tchnij moc, tchnij miłość  
i przenikaj życie me.*

[_metadata_:partType]:- "STROPHE"
**3.**  Bo całym sercem chcę odnaleźć Twą twarz.  
Bo każdą myślą chcę odnaleźć Twą twarz.  
I z całej siły chcę odnaleźć Twą twarz,  
o, Panie mój!


