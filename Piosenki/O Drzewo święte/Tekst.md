[_metadata_:partType]:- "REFRAIN"
***Ref.***  *O Drzewo święte, dla Pana ścięte,  
z Nim żeś umarło dla śmierci.*

[_metadata_:partType]:- "STROPHE"
**1.**  O najcenniejszy darze Krzyża  
podziwiamy Twą wspaniałość.  
Tyś nie jest jak drzewo rajskie,  
lecz cały piękny i miły dla smaku.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *O Drzewo święte, dla Pana ścięte,  
z Nim żeś umarło dla śmierci.*

[_metadata_:partType]:- "STROPHE"
**2.**  Tyś nam przyniosło nie śmierć, lecz życie  
nie wyrzucasz z raju, lecz do niego wprowadzasz,  
Na Ciebie wstąpił Chrystus jak na rydwan królewski,  
Diabła obalił i wyrwał nas z Jego niewoli.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *O Drzewo święte, dla Pana ścięte,  
z Nim żeś umarło dla śmierci.*

[_metadata_:partType]:- "STROPHE"
**3.**  Niegdyś przez drzewo zostaliśmy zwiedzeni,  
teraz na drzewie odtrąciliśmy starodawnego węża;  
zaiste nowa to i niezwykła zamiana:  
życie w miejsce śmierci, zamiast hańby - chwała.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *O Drzewo święte, dla Pana ścięte,  
z Nim żeś umarło dla śmierci.*

[_metadata_:partType]:- "STROPHE"
**4.**  Na Tobie jak kwiat rozkwitła najwyższa mądrość,  
ujawniając próżne przechwałki świata i głupią pychę.  
A różnorakie dobra biorące swój początek z krzyża  
Zniszczyły zarodki zła i przewrotności.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *O Drzewo święte, dla Pana ścięte,  
z Nim żeś umarło dla śmierci.*

[_metadata_:partType]:- "STROPHE"
**5.**  Dzięki Tobie przyoblekamy się w Chrystusa  
i porzucamy starego człowieka.  
Pod Tobą w jedno się gromadzimy,  
Owce Chrystusowe niebiańskiego owczarni.


