[_metadata_:partType]:- "STROPHE"
**1.**  Twoje ręce to mój ląd, wiem nie utonę   
Twoje ręce to mój brzeg, kiedy dokoła sztorm   
Twoje ręce to mój ląd pokonam drogę   
Do tych wyciągniętych rąk.

[_metadata_:partType]:- "STROPHE"
**2.**  Czuję, że już blisko jest to wytęsknione   
Wtulam się w ramiona Twe, kiedy dokoła chłód   
Pierwsza gwiazda nieba gest, wskazuje drogę   
Może dziś się zdarzy cud

[_metadata_:partType]:- "BRIDGE"
**Bridge**  Widzę Cię, jesteś tam,   
światło woła mnie

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Coraz bliżej Ciebie być, (tego pragnę)   
Chociaż czasem trzeba iść (pod prąd)   
Suchą stopą przejdę dziś (po tej wodzie)   
Twoje ręce to mój ląd*

[_metadata_:partType]:- "STROPHE"
**3.**  Twoje ręce to mój ląd, (wiem nie utonę)   
Twoje ręce to mój brzeg, (i obiecany dom)   
Jedno miejsce wolne wciąż, (zaczeka na mnie)   
Twoje ręce to mój ląd

[_metadata_:partType]:- "BRIDGE"
**Bridge**  Widzę Cię jesteś tam, (widzę Cię),   
słyszę głos znany tak (słyszę głos),   
jesteś tam widzę Cię  
światło woła mnie

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Coraz bliżej Ciebie być, (tego pragnę)   
Chociaż czasem trzeba iść (pod prąd)   
Suchą stopą przejdę dziś (po tej wodzie)   
Twoje ręce to mój ląd*

[_metadata_:partType]:- "BRIDGE"
**Bridge**  Twoje ręce to mój brzeg   
Twoje ręce to mój ląd /8x


