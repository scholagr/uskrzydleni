[_metadata_:partType]:- "STROPHE"
**1.**  Wejdźmy do Jego bram z dziękczynieniem  
U Jego tronu oddajmy cześć  
Wejdźmy do Jego bram z uwielbieniem  
Radosną Bogu śpiewajmy pieśń.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Rozraduj się w Nim, twym Stworzycielu  
Rozraduj się w Nim, Światłości Twej  
Rozraduj się w Nim, twym Zbawicielu  
Rozraduj się w Nim, wywyższaj Go chciej.*


