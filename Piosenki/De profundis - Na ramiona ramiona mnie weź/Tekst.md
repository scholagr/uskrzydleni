[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Na ramiona mnie weź   
mocą krzyża dodaj sił  
W księgę życia wpisz mnie   
do swej chwały przyjąć chciej*

[_metadata_:partType]:- "STROPHE"
**1.**  Z głębokości wznoszę głos do Ciebie  
Racz wysłuchać Panie prośby mej,  
Nakłoń ku mnie ucho Twe łaskawie.  
Usłysz modły i błagania.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Na ramiona ramiona mnie weź   
mocą krzyża dodaj sił  
W księgę życia wpisz mnie   
do swej chwały przyjąć chciej*

[_metadata_:partType]:- "STROPHE"
**2.**  Jeśli grzechów nie zapomniszPanie  
Któż przed gniewem Twym ostoi się  
Lecz ufamy, że przebaczysz winy,  
Byśmy kornie Ci służyli

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Na ramiona ramiona mnie weź   
mocą krzyża dodaj sił  
W księgę życia wpisz mnie   
do swej chwały przyjąć chciej*

[_metadata_:partType]:- "STROPHE"
**3.**  Całą ufność swą pokładam w Panu,  
Dusza moja ufa Jego słowu,  
Tęskniej czeka dusza moja Pana  
Niż jutrzenki nocne straże.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Na ramiona ramiona mnie weź   
mocą krzyża dodaj sił  
W księgę życia wpisz mnie   
do swej chwały przyjąć chciej*

[_metadata_:partType]:- "STROPHE"
**4.**  Tęskniej niż jutrzenki nocne straże  
Niechaj Pana czeka Boży lud  
Bo u Pana znajdzie zmiłowanie  
I obfite odkupienie.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Na ramiona ramiona mnie weź   
mocą krzyża dodaj sił  
W księgę życia wpisz mnie   
do swej chwały przyjąć chciej*


