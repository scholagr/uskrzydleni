[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chwalcie Pana niebios   
Chwalcie go na cytrze   
Chwalcie Króla świata,   
Bo on Bogiem jest.*

[_metadata_:partType]:- "STROPHE"
**1.**  Chwal duszo maja Pana mego Króla,   
Chcę chwalić Pana jak długo będę żył,   
Chcę śpiewać memu Bogu póki będę istniał,  
Chcę Go wysławiać, śpiewać Alleluja!

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chwalcie Pana niebios   
Chwalcie go na cytrze   
Chwalcie Króla świata,   
Bo on Bogiem jest.*

[_metadata_:partType]:- "STROPHE"
**2.**  Szczęśliwy ten, któremu Bóg jest pomocą,  
Kto ma nadzieję w Panu Bogu swym,  
W Bogu, który stworzył niebo i ziemię,  
Wszystko ci żyje śpiewa Alleluja!

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chwalcie Pana niebios   
Chwalcie go na cytrze   
Chwalcie Króla świata,   
Bo on Bogiem jest.*

[_metadata_:partType]:- "STROPHE"
**3.**  Pan Bóg króluję, wesel się ziemio,  
Bóg twój Syjonie przez pokolenia  
On dał Ci życie, On dał Ci wszystko,  
Śpiewaj Mu, wysławiaj Go Alleluja!

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Chwalcie Pana niebios   
Chwalcie go na cytrze   
Chwalcie Króla świata,   
Bo on Bogiem jest.*


