[_metadata_:partType]:- "STROPHE"
**1.**  Wierzę w Ciebie Panie coś, mnie obmył win,   
Wierzę, że człowiekiem stał się Boży Syn e   
Miłość Ci kazała krzyż na plecy brać,	   
W tabernakulum zostałeś aby z nami trwać  
Jesteś przewodnikiem nam do wieczności bram	   
Ty przygarniesz nas do siebie.

[_metadata_:partType]:- "STROPHE"
**2.**  Tyś jest moim życiem, boś Ty żywy Bóg,   
Tyś jest moją drogą, najpiękniejszą z dróg.   
To jest moją prawdą, co oświeca mnie,   
Boś odwiecznym Synem Ojca, który wszystko wie.   
Nic mnie nie zatrwoży już wśród najcięższych burz,   
Bo Ty Panie jesteś ze mną

[_metadata_:partType]:- "STROPHE"
**3.**  Tyś jest moją siłą, w Tobie moja moc,   
Tyś jest mym pokojem w najburzliwszą noc,   
Tyś jest mym ratunkiem, gdy zagraża los,   
Moją słabą ludzką rękę ujmij w swoją dłoń.   
Z Tobą przejdę poprzez świat w ciągu życia lat   
I nic złego mnie nie spotka

[_metadata_:partType]:- "STROPHE"
**4.**  Tobie Boże miłość, wiarę swoją dam.   
W Tobie Synu Boży ufność swoją mam.   
Duchu Święty Boże w serce moje zstąp   
I miłości Bożej ziarno   
rzuć w me serce w głąb.   
W duszy mojej rozpal żar, siedmioraki dar, Daj mi stać się Bożą rolą


