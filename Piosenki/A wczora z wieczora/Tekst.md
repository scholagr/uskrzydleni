[_metadata_:partType]:- "STROPHE"
**1.**  A wczora z wieczora,   
a wczora z wieczora z niebieskiego dwora,   
z niebieskiego dwora.   
Przyszła nam nowina,   
przyszła nam nowina: Panna rodzi Syna,   
Panna rodzi Syna.

[_metadata_:partType]:- "STROPHE"
**2.**  Boga prawdziwego,  
nieogarnionego.   
Za wyrokiem Boskim,   
w Betlejem żydowskim.

[_metadata_:partType]:- "STROPHE"
**3.**  Pastuszkowie mali   
w polu wtenczas spali,   
gdy Anioł z północy   
światłość z nieba toczy.

[_metadata_:partType]:- "STROPHE"
**4.**  Chwałę oznajmując,   
szopę pokazując,   
chwałę Boga tego,   
dziś nam zrodzonego.

[_metadata_:partType]:- "STROPHE"
**5.**  Chwałę oznajmując,   
szopę pokazując,   
gdzie Panna z Dzieciątkiem,  
z wołem i oślątkiem.

[_metadata_:partType]:- "STROPHE"
**6.**  I z Józefem starym,  
nad Jezusem małym,  
chwałę Boga tego,  
dziś nadrodzonego.

[_metadata_:partType]:- "STROPHE"
**7.**  "Tam Panna Dzieciątko,   
miłe Niemowlątko,   
uwija w pieluszki,  
pośpieszcie pastuszki!"

[_metadata_:partType]:- "STROPHE"
**8.**  Natychmiast pastuszy   
śpieszą z całej duszy,   
weseli bez miary,   
niosą z sobą dary.

[_metadata_:partType]:- "STROPHE"
**9.**  Mądrości druhowie,   
z daleka królowie,   
pragną widzieć swego   
Stwórcę przedwiecznego.

[_metadata_:partType]:- "STROPHE"
**10.**  Dziś Mu pokłon dają   
w ciele oglądają.   
Każdy się dziwuje,   
że Bóg nas miłuje.

[_metadata_:partType]:- "STROPHE"
**11.**  I my też pośpieszmy,   
Jezusa ucieszmy   
ze serca darami:   
modlitwa, cnotami.

[_metadata_:partType]:- "STROPHE"
**12.**  Jezu najmilejszy,   
ze wszech najwdzięczniejszy,   
zmiłuj się nad nami  
grzesznymi sługami.


