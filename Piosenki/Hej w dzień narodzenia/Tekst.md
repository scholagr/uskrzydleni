[_metadata_:partType]:- "STROPHE"
**1.**  Hej, w dzień narodzenia Syna Jedynego   
Ojca Przedwiecznego, Boga Prawdziwego,

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Wesoło śpiewajmy, chwałę Bogu dajmy,  
Hej, kolęda, kolęda!*

[_metadata_:partType]:- "STROPHE"
**2.**  Panna porodziła niebieskie Dzieciątko,  
W żłobie położyła małe Pacholątko.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Pasterze śpiewają, na multankach grają,  
Hej, kolęda, kolęda!*

[_metadata_:partType]:- "STROPHE"
**3.**  Skoro pastuszkowie o tym usłyszeli,  
Zaraz do Betlejem czym prędzej bieżeli,

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Witają Dzieciątko, małe Pacholątko,  
Hej, kolęda, kolęda!*

[_metadata_:partType]:- "STROPHE"
**4.**  A Klimas porwawszy barana jednego,   
A Staszek czem prędzej schwytawszy drugiego,

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Tych bydlątek parę, Panu na ofiarę,   
Hej, kolęda, kolęda!*

[_metadata_:partType]:- "STROPHE"
**5.**  Kuba nieboraczek nierychło przybieżał,   
Śpieszno ni tak ni siak, wszystkiego odbieżał,

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Panu nie miał co dać kazali mu śpiewać.   
Hej, kolęda, kolęda!*

[_metadata_:partType]:- "STROPHE"
**6.**  Zdobył tak wdzięcznego głosu baraniego.   
Aż się Józef Stary przestraszył od niego,

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Już uciekać myśli, ale drudzy przyszli,   
Hej, kolęda, kolęda!*


