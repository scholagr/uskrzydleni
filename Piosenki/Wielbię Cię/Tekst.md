[_metadata_:partType]:- "STROPHE"
**1.**  Wielbię Cię,  
całe życie Ci oddaję   
Wielbię Cię,  
choć uczynki moje małe.

[_metadata_:partType]:- "STROPHE"
**2.**  Wielbię Cię,  
jesteś bliżej niż ja sobie   
Wielbię Cię,  
dałeś wszystko w Swoim Słowie.

[_metadata_:partType]:- "STROPHE"
**3.**  Wielbię Cię,  
noszę w sercu Twoje znamię   
Wielbię Cię,  
Ty znasz imię me na pamięć.

[_metadata_:partType]:- "STROPHE"
**4.**  Wielbię Cię,  
całym sercem, duszą, ciałem   
Wielbię Cię,  
oto jestem dla Twej chwały.


