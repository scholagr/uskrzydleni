[_metadata_:partType]:- "INTRO"
**Wej.**  

[_metadata_:partType]:- "STROPHE"
**1.**  Północ już była gdy się zjawiła  
nad bliską doliną jasna łuna,  
którą zoczywszy i zobaczywszy,  
krzyknął mocno Wojtek na Szymona:

[_metadata_:partType]:- "BRIDGE"
**Bridge**  Szymonie kochany,   
znak to niewidziany,  
że całe niebo goreje.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Na braci zawołaj, niechaj wstawają  
Kuba i Mikołaj niechaj wypędzają  
barany i copy,  
koźlęta i skopy  
zamknione.*

[_metadata_:partType]:- "STROPHE"
**2.**  Leżąc w stodole,  
patrząc na pole,  
ujrzał Bartosz stary Anioły,  
które wdzięcznymi  
głosami swymi  
okrzyknęły ziemskie padoły:

[_metadata_:partType]:- "BRIDGE"
**Bridge**  na niebie niech chwała  
Bogu będzie trwała,  
a ludziom pokój na ziemi!

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Na braci zawołaj, niechaj wstawają  
Kuba i Mikołaj niechaj wypędzają  
barany i copy,  
koźlęta i skopy  
zamknione.*


