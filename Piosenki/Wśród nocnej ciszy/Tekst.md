[_metadata_:partType]:- "STROPHE"
**1.**  Wśród nocnej ciszy   
głos się rozchodzi:   
Wstańcie, pasterze,   
Bóg się wam rodzi!

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Czym prędzej się wybierajcie,  
Do Betlejem pospieszajcie   
Przywitać Pana.*

[_metadata_:partType]:- "STROPHE"
**2.**  Poszli, znaleźli   
Dzieciątko w żłobie  
z wszystkimi znaki,  
 danymi sobie.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Jako Bogu cześć Mu dali,  
a witając zawołali  
z wielkiej radości, z wielkiej radości*

[_metadata_:partType]:- "STROPHE"
**3.**  Ach witaj, Zbawco,   
z dawna żądany,  
wiele tysięcy   
lat wyglądany.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Na Ciebie króle, prorocy  
czekali, a Tyś tej nocy  
nam się objawił.*

[_metadata_:partType]:- "STROPHE"
**4.**  I my czekamy   
na Ciebie Pana,  
a skoro przyjdziesz   
na głos kapłana,

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Padniemy na twarz przed Tobą,  
I wierząc, żeś jest pod osłoną  
chleba i wina.*


