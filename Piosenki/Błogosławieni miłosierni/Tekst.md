[_metadata_:partType]:- "STROPHE"
**1.**  Wznoszę swe oczy ku górom, skąd  
Przyjdzie mi pomoc;  
Pomoc od Pana, wszak Bogiem On  
Miłosiernym jest!

[_metadata_:partType]:- "STROPHE"
**2.**  Kiedy zbłądzimy, sam szuka nas,  
By w swe ramiona wziąć,  
Rany uleczyć Krwią swoich ran,  
Nowe życie tchnąć!

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Błogosławieni miłosierni,  
albowiem oni miłosierdzia dostąpią  
  
2x*

[_metadata_:partType]:- "STROPHE"
**3.**  Gdyby nam Pan nie odpuścił win,  
Któż ostać by się mógł?  
Lecz On przebacza, przeto i my  
Czyńmy jak nasz Bóg!

[_metadata_:partType]:- "STROPHE"
**4.**  Pan Syna Krwią zmazał wszelki dług,  
Syn z grobu żywy wstał;  
„Panem jest Jezus” – mówi w nas Duch.  
Niech to widzi świat!

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Błogosławieni miłosierni,  
albowiem oni miłosierdzia dostąpią  
  
2x*

[_metadata_:partType]:- "BRIDGE"
**Bridge**  Więc odrzuć lęk i wiernym bądź,  
Swe troski w Panu złóż  
I ufaj, bo zmartwychwstał i wciąż  
Żyje Pan, Twój Bóg!

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Błogosławieni miłosierni,  
albowiem oni miłosierdzia dostąpią  
  
2x*


