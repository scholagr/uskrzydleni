[_metadata_:partType]:- "STROPHE"
**1.**  Będę śpiewał o sprawiedliwości,  
Tobie chcę śpiewać, o Panie!  
Będę szedł drogą nieskalaną.  
Kiedy Ty przyjdziesz do mnie, Boże?

[_metadata_:partType]:- "STROPHE"
**2.**  Będę chodził z mym sercem niewinnym  
we wnętrzu mojego domu.  
Nie będę zwracał moich oczu  
ku żadnej sprawie niegodziwej.


