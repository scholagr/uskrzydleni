[_metadata_:partType]:- "STROPHE"
**1.**  Dziś nie musisz bać się Boga  
jest taki mały  
bezbronny jak sen

[_metadata_:partType]:- "STROPHE"
**2.**  dziś nie musisz prosić o nic Boga  
dziś Bóg jest , taki jak ty

[_metadata_:partType]:- "BRIDGE"
**Bridge**  możesz wziąć boga na ręce  
ukołysać, utulić, zaśpiewać przed snem  
a może wtedy On pomoże zrozumieć ci więcej  
i pojmiesz choć na chwile po co jesteś tu

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *póki co, Ty śpij Jezu, spij  
Śnij Jezu, śnij  
jeszcze mnie wiesz co sicie ze mna czeka  
odpocznij, bo droga daleka*

[_metadata_:partType]:- "STROPHE"
**3.**  dziś nie musisz gniewać się na Boga  
jest taki mały, bezbronny jak sen  
dziś nie musisz prosić o nic Boga

[_metadata_:partType]:- "STROPHE"
**4.**  dziś nie musisz prosić o nic Boga  
dziś Bóg jest , taki jak ty

[_metadata_:partType]:- "BRIDGE"
**Bridge**  możesz wziąć boga na ręce  
ukołysać, utulić, zaśpiewać przed snem  
a może wtedy On pomoże zrozumieć ci więcej  
i pojmiesz choć na chwile po co jesteś tu

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *póki co, Ty śpij Jezu, spij  
Śnij Jezu, śnij  
jeszcze mnie wiesz co sicie ze mna czeka  
odpocznij, bo droga daleka*

[_metadata_:partType]:- "STROPHE"
**5.**  dziś nie musisz bać się Boga  
jest taki mały  
bezbronny jak sen  
dziś nie musisz nawet wierzyć w Boga  
Dziś Bóg jest taki jak ty


