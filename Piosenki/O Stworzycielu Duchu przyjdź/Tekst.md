[_metadata_:partType]:- "STROPHE"
**1.**  O Stworzycielu, Duchu, przyjdź,  
Nawiedź dusz wiernych Tobie krąg.  
Niebieską łaskę zesłać racz  
Sercom, co dziełem są Twych rąk.

[_metadata_:partType]:- "STROPHE"
**2.**  Pocieszycielem jesteś zwan  
I najwyższego Boga dar.  
Tyś namaszczeniem naszych dusz,  
Zdrój żywy, miłość, ognia żar.

[_metadata_:partType]:- "STROPHE"
**3.**  Ty darzysz łaską siedemkroć,  
Bo moc z prawicy Ojca masz,  
Przez Boga obiecany nam,  
Mową wzbogacasz język nasz.

[_metadata_:partType]:- "STROPHE"
**4.**  Światłem rozjaśnij naszą myśl,  
W serca nam miłość świętą wlej  
I wątłą słabość naszych ciał  
Pokrzep stałością mocy Twej.

[_metadata_:partType]:- "STROPHE"
**5.**  Nieprzyjaciela odpędź w dal  
I Twym pokojem obdarz wraz.  
Niech w drodze za przewodem Twym  
Miniemy zło, co kusi nas.

[_metadata_:partType]:- "STROPHE"
**6.**  Daj nam przez Ciebie Ojca znać,  
Daj, by i Syn poznany był.  
I Ciebie, jedno tchnienie Dwóch,  
Niech wyznajemy z wszystkich sił.

[_metadata_:partType]:- "STROPHE"
**7.**  Niech Bogu Ojcu chwała brzmi,  
Synowi, który zmartwychwstał,  
I Temu, co pociesza nas,  
Niech hołd wieczystych płynie chwał. Amen.


