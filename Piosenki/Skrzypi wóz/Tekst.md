[_metadata_:partType]:- "STROPHE"
**1.**  Skrzypi wóz, wielki mróz  
wielki mróz na ziemi. 2x  
Trzej królowie jadą  
złoto, mirrę kładą.  
Hej, kolęda, kolęda. 2x

[_metadata_:partType]:- "STROPHE"
**2.**  A komu takiemu  
Dzieciątku małemu. 2x  
Cóż to za Dzieciątko,  
musi być paniątko.  
Hej, kolęda, kolęda. 2x

[_metadata_:partType]:- "STROPHE"
**3.**  To Jezus maluśki,  
to Dzieciątko krasne. 2x  
Cichutkie, malutkie  
jak słoneczko jasne.  
Hej, kolęda, kolęda. 2x

[_metadata_:partType]:- "STROPHE"
**4.**  Pasterze na lirze,  
na skrzypeczkach grali. 2x  
beztroscy do szopy  
prędko podążali.  
Hej, kolęda, kolęda. 2x

[_metadata_:partType]:- "STROPHE"
**5.**  Pójdę ja do Niego  
poproszę od Niego. 2x  
,,Daj nam Boże Dziecię  
pokój na tym świecie''  
Hej, kolęda, kolęda.


