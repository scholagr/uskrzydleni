[_metadata_:partType]:- "STROPHE"
**1.**  Uwielbiam Imię Twoje, Panie.  
Wywyższam Cię i daję Ci cześć.  
W przedsionku chwały Twej staję  
Z radością śpiewam Ci pieśń.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *O Panie Jezu chcę wyznać, że  
Ja kocham Ciebie,  
Ty zmieniasz mnie.  
Chcę Ci dziękować   
ze wszystkich sił.  
Dajesz mi siebie bym   
na wieki żył.*


