[_metadata_:partType]:- "STROPHE"
**1.**  D &#124; D &#124; C6 &#124; D/h \ C6  
D &#124; D &#124; C6 &#124; D/h \ C6

[_metadata_:partType]:- "BRIDGE"
**Bridge**  G &#124; A  
B &#124; C

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *D &#124; A &#124; C &#124; G  
D &#124; A &#124; C &#124; G  
D &#124; A &#124; C &#124; G  
D &#124; A &#124; B &#124; C*

[_metadata_:partType]:- "STROPHE"
**2.**  D &#124; D &#124; C6 &#124; D/h \ C6  
D &#124; D &#124; C6 &#124; D/h \ C6

[_metadata_:partType]:- "BRIDGE"
**Bridge**  G &#124; A  
B &#124; C

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *D &#124; A &#124; C &#124; G  
D &#124; A &#124; C &#124; G  
D &#124; A &#124; C &#124; G  
D &#124; A &#124; B &#124; C*

[_metadata_:partType]:- "BRIDGE"
**Bridge**  D \ D \ D \ F &#124; F \ G \ G &#124; -  
D \ D \ D \ C6 &#124; C6 \ D/h \ D/h &#124; -  
D \ D \ D \ F &#124; F \ G \ G &#124; -  
D \ D \ D \ C6 &#124; C6 \ D/h \ D/h &#124; -

[_metadata_:partType]:- "OUTRO"
**Wyj.**  D &#124; C6 &#124; D/h &#124; D/b


