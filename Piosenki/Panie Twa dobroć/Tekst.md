[_metadata_:partType]:- "STROPHE"
**1.**  Panie Twa dobroć i łaska trwa na wieki wieków   
Panie Twa dobroć i łaska trwa na wieki wieków

[_metadata_:partType]:- "BRIDGE"
**Bridge**  Wszystkie narody oddają Ci cześć,   
w każdym języku wyznają Ci, że:

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Wielbimy Cię alleluja alleluja,   
wielbimy Cię za dobroć Twą  
Wielbimy Cię alleluja alleluja,   
wielbimy Cię za dobroć Twą*

[_metadata_:partType]:- "STROPHE"
**2.**  Panie Twa dobroć i łaska trwa na wieki wieków   
Panie Twa dobroć i łaska trwa na wieki wieków

[_metadata_:partType]:- "BRIDGE"
**Bridge**  Wszystkie narody oddają Ci cześć,   
w każdym języku wyznają Ci, że:

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Wielbimy Cię alleluja alleluja,   
wielbimy Cię za dobroć Twą /2x*

[_metadata_:partType]:- "BRIDGE"
**Bridge**  Każdego dnia.  
Dobry jest, dobry jest, nasz Bóg, nasz Bóg!  
Dobry jest, dobry jest

[_metadata_:partType]:- "OUTRO"
**Wyj.**  Dobry jest cały czas, cały czas, dobry jest! /4x


