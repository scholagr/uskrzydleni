[_metadata_:partType]:- "STROPHE"
**1.**  Nosi ciebie z miasta i do miasta,  
Popatrz na mnie, bracie, ja też krwawię,  
Od wieków już nie tańczę na zabawie,  
Lecz nie pora na nas.

[_metadata_:partType]:- "STROPHE"
**2.**  Jedna zła dziewczyna to nie wszystko,  
Po ziemi chodzą Weroniki śliczne,  
Wyciągnij rękę, nie noś głowy nisko,  
Bo nie pora na nas...

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Uwierz mi - warto żyć,  
Życie to jest taka świetna sprawa,  
Póki trwa, póki trwa,  
Póki jeszcze kipi jak karnawał.*

[_metadata_:partType]:- "STROPHE"
**3.**  Po ziemi chodzą Weroniki śliczne,  
Ich dusze są jak ciepłe winogrona,  
Zachowaj dla nich swoje serce czyste,  
Wracaj, wracaj do nas.

[_metadata_:partType]:- "STROPHE"
**4.**  Nie kalecz się, braciszku, nawet różą,  
Odjazdu nie nazywaj pięknym balem,  
Pozostaw pusty peron przed podróżą,  
Bo nie pora na nas...


