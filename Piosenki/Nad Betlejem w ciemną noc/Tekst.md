[_metadata_:partType]:- "STROPHE"
**1.**  Nad Betlejem w ciemną noc  
śpiewał pieśń aniołów chór.  
Ich radosny, cudny głos  
odbijało echo gór.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Gloria  
in exscelsis Deo!*

[_metadata_:partType]:- "STROPHE"
**2.**  Pastuszkowie, jaką pieśń  
słyszeliście nocy tej?  
Jakaż to radosna wieść  
była tam natchnieniem jej?

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Gloria  
in exscelsis Deo!*

[_metadata_:partType]:- "STROPHE"
**3.**  Do Betlejem prędko spiesz,  
zostaw stada pośród pól,  
gdyż anielska głosi wieść,  
że się tam narodził Król.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Gloria  
in exscelsis Deo!*

[_metadata_:partType]:- "STROPHE"
**4.**  W twardym żłobie leży tam  
Jezus- nieba, ziemi Pan.  
Chciejmy mu w pokorze wznieść  
uwielbienie, chwałę, cześć!

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Gloria  
in exscelsis Deo!*


