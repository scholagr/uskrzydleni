[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Miłość jest cierpliwa  
Miłość jest łaskawa  
Miłość jest największa  
Miłość nadaje sens  
Miłość jest niepojęta  
Miłość nie, nie zazdrości  
Miłość wszystko przetrzyma  
Miłość największa jest*

[_metadata_:partType]:- "STROPHE"
**1.**  Gdybym mówił językami  
I nawet gdybym miał wszelką wiedzę i wszelką wiarę  
Tak iż bym góry przenosił  
A miłości bym nie miał  
Stałbym się jak miedź brzęcząca  
albo cymbał brzmiący

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Miłość jest cierpliwa  
Miłość jest łaskawa  
Miłość jest największa  
Miłość nadaje sens  
Miłość jest niepojęta  
Miłość nie, nie zazdrości  
Miłość wszystko przetrzyma  
Miłość największa jest*

[_metadata_:partType]:- "BRIDGE"
**Bridge**  Miłość co daje nie zabiera  
Ta właśnie miłość daje życiu sens  
  
4x

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Miłość jest cierpliwa  
Miłość jest łaskawa  
Miłość jest największa  
Miłość nadaje sens  
Miłość jest niepojęta  
Miłość nie, nie zazdrości  
Miłość wszystko przetrzyma  
Miłość największa jest*

[_metadata_:partType]:- "BRIDGE"
**Bridge**  Daj nam Panie poznać miłość  
Wlej ją w nasze serca dziś   
  
6x


