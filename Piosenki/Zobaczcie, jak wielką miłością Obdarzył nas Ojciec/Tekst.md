[_metadata_:partType]:- "STROPHE"
**1.**  Zobaczcie, jak wielką miłością  
Obdarzył nas Ojciec,

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Śpiewajmy dobremu Bogu:Alleluja  
Śpiewajmy dobremu Bogu:Alleluja*

[_metadata_:partType]:- "STROPHE"
**2.**  On nazwał nas swymi dziećmi  
I rzeczywiście nimi jesteśmy.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Śpiewajmy dobremu Bogu:Alleluja  
Śpiewajmy dobremu Bogu:Alleluja*

[_metadata_:partType]:- "STROPHE"
**3.**  Wiemy, że gdy się objawi,  
Będziemy do Niego podobni

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Śpiewajmy dobremu Bogu:Alleluja  
Śpiewajmy dobremu Bogu:Alleluja*

[_metadata_:partType]:- "STROPHE"
**4.**  Będziemy widzieć Pana,  
Takiego jakim jest

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Śpiewajmy dobremu Bogu:Alleluja  
Śpiewajmy dobremu Bogu:Alleluja*

[_metadata_:partType]:- "STROPHE"
**5.**  Każdy kto ma w Nim nadzieję,  
Podobnie jak On jest święty


