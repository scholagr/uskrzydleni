[_metadata_:partType]:- "STROPHE"
**1.**  Jezus malusieńki wśród stajenki  
Płacze z zimna nie dała mu matula sukienki

[_metadata_:partType]:- "STROPHE"
**2.**  Bo uboga była, rąbek z głowy zdjęła,  
w który Dziecię owinąwszy, siankiem go okryła

[_metadata_:partType]:- "STROPHE"
**3.**  Nie ma kolebeczki, ani poduszeczki,  
We żłobie Mu położyła siana pod główeczki.

[_metadata_:partType]:- "STROPHE"
**4.**  Gdy Dziecina kwili,patrzy w każdej chwili.  
Na Dzieciątko boskie w żłóbku,oko jej nie myli.

[_metadata_:partType]:- "STROPHE"
**5.**  Panienka truchleje, a mówiąc łzy leje  
O mój Synu ! Wola Twoja, nie moja się dzieje.

[_metadata_:partType]:- "STROPHE"
**6.**  Tylko nie płacz,proszę,bo żalu nie zniosę,  
Dosyć go mam z męki Twojej, którą w sercu noszę.

[_metadata_:partType]:- "STROPHE"
**7.**  Pokłon oddawajmy, Bogiem go wyznajmy  
To Dzieciątko ubożuchne ludziom ogłaszajmy.

[_metadata_:partType]:- "STROPHE"
**8.**  Józefie stajenki, daj z ogniem fajerki  
Grzać Dziecinę, sam co prędzej podpieraj stajenki.

[_metadata_:partType]:- "STROPHE"
**9.**  Niech Go wszyscy znają, serdecznie kochają  
Za tak wielkie poniżenie chwałę Mu oddają


