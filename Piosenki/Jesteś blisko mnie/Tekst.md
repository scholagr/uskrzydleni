[_metadata_:partType]:- "STROPHE"
**1.**  Jesteś blisko mnie,   
Tęsknię za Duchem Twym.   
Kocham kroki Twe   
i wiem jak pukasz do drzwi.

[_metadata_:partType]:- "BRIDGE"
**Bridge**  Przychodzisz jak ciepły wiatr,  
Otwieram się i czuję znów, że

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Twoja miłość jak ciepły deszcz,  
Twoja miłość jak morze gwiazd   
za dnia, Twoja miłość sprawia, że Nieskończenie dobry   
Święty Duch Ogarnia mnie*

[_metadata_:partType]:- "STROPHE"
**2.**  Kocham kroki Twe   
i wiem jak pukasz do drzwi.

[_metadata_:partType]:- "BRIDGE"
**Bridge**  Przychodzisz jak ciepły wiatr,  
Otwieram się i czuję znów, że

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Twoja miłość jak ciepły deszcz,  
Twoja miłość jak morze   
gwiazd za dnia  
Twoja miłość sprawia, że   
Nieskończenie dobry   
Święty Duch Ogarnia mnie*


