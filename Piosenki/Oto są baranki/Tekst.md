[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Oto są baranki młode,   
oto ci, co zawołali alleluja!   
Dopiero przyszli do zdrojów, światłością się napełnili,   
Alleluja, alleluja!*

[_metadata_:partType]:- "STROPHE"
**1.**  Na Baranka Pańskich godach,   
W szat świątecznych czystej bieli,   
Po krwawego morza wodach   
Nieśmy Panu pieśń weseli.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Oto są baranki młode,   
oto ci, co zawołali alleluja!   
Dopiero przyszli do zdrojów, światłością się napełnili,   
Alleluja, alleluja!*

[_metadata_:partType]:- "STROPHE"
**2.**  W swej miłości wiekuistej  
On nas swoją Krwią częstuje,  
Nam też Ciało swe przeczyste  
Chrystus kapłan ofiaruje.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Oto są baranki młode,   
oto ci, co zawołali alleluja!   
Dopiero przyszli do zdrojów, światłością się napełnili,   
Alleluja, alleluja!*

[_metadata_:partType]:- "STROPHE"
**3.**  Na drzwi świętą Krwią skropione  
Anioł mściciel z lękiem wziera,  
Pędzi morze rozdzielone,  
Wrogów w nurtach swych pożera.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Oto są baranki młode,   
oto ci, co zawołali alleluja!   
Dopiero przyszli do zdrojów, światłością się napełnili,   
Alleluja, alleluja!*

[_metadata_:partType]:- "STROPHE"
**4.**  Już nam Paschą Tyś, o Chryste,  
Wielkanocną też ofiarą,  
Tyś Przaśniki nasze czyste   
Dla dusz prostych z szczerą wiarą

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Oto są baranki młode,   
oto ci, co zawołali alleluja!   
Dopiero przyszli do zdrojów, światłością się napełnili,   
Alleluja, alleluja!*

[_metadata_:partType]:- "STROPHE"
**5.**  O Ofiaro niebios święta,  
Ty moc piekła pokonywasz,  
Zrywasz ciężkie śmierci pęta,  
Wieniec życia nam zdobywasz.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Oto są baranki młode,   
oto ci, co zawołali alleluja!   
Dopiero przyszli do zdrojów, światłością się napełnili,   
Alleluja, alleluja!*

[_metadata_:partType]:- "STROPHE"
**6.**  Chrystus piekło pogromiwszy  
Swój zwycięski znak roztacza,  
Niebo ludziom otworzywszy  
Króla mroków w więzy wtłacza.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Oto są baranki młode,   
oto ci, co zawołali alleluja!   
Dopiero przyszli do zdrojów, światłością się napełnili,   
Alleluja, alleluja!*

[_metadata_:partType]:- "STROPHE"
**7.**  Byś nam wiecznie, Jezu drogi,   
Wielkanocną był radością,  
Strzeż od grzechu śmierci srogiej   
odrodzonych Twą miłością.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Oto są baranki młode,   
oto ci, co zawołali alleluja!   
Dopiero przyszli do zdrojów, światłością się napełnili,   
Alleluja, alleluja!*

[_metadata_:partType]:- "STROPHE"
**8.**  Chwała Ojcu i Synowi,   
który z martwych żywy wstaje  
I Świętemu też duchowi   
niech na wieki nie ustaje.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Oto są baranki młode,   
oto ci, co zawołali alleluja!   
Dopiero przyszli do zdrojów, światłością się napełnili,   
Alleluja, alleluja!*


