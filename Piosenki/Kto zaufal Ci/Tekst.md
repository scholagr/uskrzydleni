[_metadata_:partType]:- "STROPHE"
**1.**  Kto zaufał Ci, ten uwierzył,   
że nie zawstydzi go nic  
Moc Twego ramienia jest skałą schronienia  
kiedy skrada się wróg.

[_metadata_:partType]:- "STROPHE"
**2.**  Szczęśliwy ten, kto uwierzył, nie zawstydzi go nic  
Moc Twego ramienia jest skałą schronienia  
Kiedy skrada się wróg

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Zawsze będę ufać,   
pomnażać wszelką Twą chwałę  
I głosić Twą sprawiedliwość,  
gdy sięga ona aż do nieba*

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Ubierasz mą duszę w dostojność  
To co umarłe ożywiasz  
Raduje się moje serce, wierność Wszechmocnego   
nieustannie opowiadać chcę.*


