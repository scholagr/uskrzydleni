[_metadata_:partType]:- "STROPHE"
**1.**  A &#124; h7 &#124; fis7 (capo 3)  
D \ E7 &#124; A   
A &#124; D &#124; fis7  
D \ A &#124; E7 \ A

[_metadata_:partType]:- "STROPHE"
**2.**  A &#124; h7 &#124; fis7  
D \ E7 &#124; A   
A &#124; D &#124; fis7  
D \ A &#124; E7 \ A  
  
fis7 222242  
h7 23242  
E7 001020

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *F \ G &#124; a  
F \ G &#124; F \ C  
F \ G &#124; a  
d9 [xx2211] &#124; F7+?*

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *F \ G &#124; a  
F \ G &#124; F \ C  
F \ G &#124; a  
d9   
a7/e   
F7+ &#124; E2-7*


