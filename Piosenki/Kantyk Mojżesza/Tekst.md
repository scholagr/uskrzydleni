[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Śpiewajcie Panu, bo wielka jego moc i chwałą  
On z niewoli zła swój lud ocala*

[_metadata_:partType]:- "STROPHE"
**1.**  Będę śpiewał na cześć Pana,  
który wspaniale potęgę okazał, gdy  
konia i jeźdźca pogrążył w morskiej przepaści

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Śpiewajcie Panu, bo wielka jego moc i chwałą  
On z niewoli zła swój lud ocala*

[_metadata_:partType]:- "STROPHE"
**2.**  Pan jest moją mocą i źródłem męstwa, Jemu zawdzięczam  
moje ocalenie, On Bogiem moim uwielbiać Go będę.  
On Bogiem Ojca mego, będę Go wywyższał

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Śpiewajcie Panu, bo wielka jego moc i chwałą  
On z niewoli zła swój lud ocala*

[_metadata_:partType]:- "STROPHE"
**3.**  Rzucił w morze rydwany faraona i jego wojsko,  
wybrani wodzowie zginęli w Morzu Czerwonym,  
przepaści ich ogarnęły, jak głaz runęli w głębinę.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Śpiewajcie Panu, bo wielka jego moc i chwałą  
On z niewoli zła swój lud ocala*

[_metadata_:partType]:- "STROPHE"
**4.**  Wyprowadziłeś swój lud i osadziłeś na górze  
Twojego dziedzictwa, w miejscu, które uczyniłeś swym mieszkaniem,  
Pan Bóg jest Królem na zawsze i na wieki.


