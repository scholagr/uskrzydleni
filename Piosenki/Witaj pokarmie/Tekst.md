[_metadata_:partType]:- "STROPHE"
**1.**  Witaj pokarmie w którym niezmierzony  
Nieba i ziemie Twórca jest zamkniony  
Witaj napoju zupełnie gaszący  
Umysł pragnący

[_metadata_:partType]:- "STROPHE"
**2.**  Witaj krynico wszystkiego dobrego  
Gdy bowiem w sobie masz Boga samego  
Znasz ludziom wszystkie jego wszechmocności  
Niesiesz godności

[_metadata_:partType]:- "STROPHE"
**3.**  Witaj z niebiosów manno padająca  
Rozkoszny w sercu naszym smak czyniąca  
Wszystko na świecie co jedno smakuje  
W tym się najduje

[_metadata_:partType]:- "STROPHE"
**4.**  Witaj rozkoszne z ogrodu rajskiego  
Drzewo owocu pełne żywiącego  
Kto cię skosztuje śmierci się nie boi  
Choć nad nim stoi

[_metadata_:partType]:- "STROPHE"
**5.**  Witaj jedyna serc ludzkich radości  
Witaj strapionych wszelka łaskawości  
Ciebie dziś moje łzy słodkie szukają  
K Tobie wołają


