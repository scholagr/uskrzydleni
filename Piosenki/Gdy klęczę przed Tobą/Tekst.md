[_metadata_:partType]:- "STROPHE"
**1.**  Gdy klęczę przed Tobą,   
modlę się i składam hołd,   
Weź ten dzień, uczyń go Twym,  
i we mnie miłość wznieć.

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Ave Maria, gratia plena,  
Dominus tecum, benedicta tu  
Ave Maria, gratia plena,  
Dominus tecum, benedicta tu*

[_metadata_:partType]:- "STROPHE"
**2.**  Wszystko Tobie daję,  
każdy sen i każdą myśl.  
Matko Boga, Matko moja,  
wznieś je przed Pana tron!

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *Ave Maria, gratia plena,  
Dominus tecum, benedicta tu  
Ave Maria, gratia plena,  
Dominus tecum, benedicta tu*

[_metadata_:partType]:- "STROPHE"
**3.**  Gdy klęczę przed Tobą,  
widzę Twą radosną twarz.  
Każda myśl, każde słowo,  
niech spocznie w dłoniach Twych.


