[_metadata_:partType]:- "STROPHE"
**1.**  Pewnej nocy łzy z oczu mych   
otarł dłonią swą Jezus   
i powiedział mi: „Nie martw się,   
Jam przy boku jest twym.”   
Potem spojrzał na grzeszny świat  
pogrążony w ciemności,  
i zwracając się do mnie,  
pełen smutku tak rzekł:

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *„Powiedz ludziom,   
że kocham ich,   
że się o nich wciąż troszczę,   
jeśli zeszli już z moich dróg,   
powiedz, że szukam ich."*

[_metadata_:partType]:- "STROPHE"
**2.**  Gdy na wzgórzu Golgoty  
Życie za nich oddałem  
To umarłem za wszystkich  
Aby każdy mógł żyć  
Nie zapomnę tej chwili  
Gdy mnie spotkał mój Jezus  
Wtedy byłem jak ślepy  
On przywrócił mi wzrok

[_metadata_:partType]:- "REFRAIN"
***Ref.***  *„Powiedz ludziom,   
że kocham ich,   
że się o nich wciąż troszczę,   
jeśli zeszli już z moich dróg,   
powiedz, że szukam ich."*


