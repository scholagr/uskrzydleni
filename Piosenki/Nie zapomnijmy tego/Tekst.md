[_metadata_:partType]:- "INTRO"
**Wej.**  Zanim się skończy ciepło  
I gwiazda skrzydła zwinie  
I trzeba będzie znowu  
Wędrować przez pustynię  
Dotknijmy siebie dłonią  
Jakbyśmy dotykali Tego  
Co się narodził  
I światło w nas zapalił

[_metadata_:partType]:- "STROPHE"
**1.**  Nie zapomnijmy tego  
Dziecinnie bezbronnego  
Bezbronnie naiwnego  
Ten blask był przecież w nas.

[_metadata_:partType]:- "STROPHE"
**2.**  Nie zapomnijmy ciepła  
Co może już uciekło  
Zgubiło się w tej drodze  
Ale się wrócić może  
Bo nieskończony czas

[_metadata_:partType]:- "STROPHE"
**3.**  Gwiazdy pachnącej sianem  
Prezentów niespodzianych  
Przyjaciół zapomnianych  
Co domu drzwi otworzą  
Aby powitać nas

[_metadata_:partType]:- "OUTRO"
**Wyj.**  Bagaże spakowane  
Matka otula dziecię  
Osiołek nogą grzebie  
Ma długą drogę przecież  
Ciemności w drzwi stukają  
Lodowe mają twarze  
Nie zapomnijmy tego  
Żeśmy tu byli razem.


